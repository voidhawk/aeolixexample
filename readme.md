## Goal (Bilder und Icons fehlen)
This tutorial shows the steps to receive ETA information using the ATOS CE (Channel Environment).

This sample could be used to create a trip, a subscription and gives the possibility to send position information. This 3 steps are needed to receive ETA information (Using the PTV ETA service).

## Environment (Trademarks fehlen)
I´m developing under Win10 and as IDE I use VisualStudioCode. As additional tools I use npm, nodejs and Angular.

## Installation
```shell
- first steps
-- download the zip file and extract the data into a folder (.\AeolixExample)
-- switch to your selected directory
-- open a command prompt and run
--- npm install
--- npm run build
```
### In a second command line call start node-server
The command "npm start" executes the node server. The port is configured to 3000, if necessary you could change this in the file "YourInstallationPath\Aeolix\AeolixExample\bin\www".
```shell
npm start
```

After running the command you should see something like the picture above.
![Start npm/node](/docs/bitmap/StartNode.png)


In a browser (I use Chrome)
```shell
localhost:3000
```
After starting you should see something like this. 

![Start AeolixExample](/docs/bitmap/Start.png)

The language is set to the default language of your browser, if not suitable please use the language combobox.


## How is the example used
- Infos und Bilder fehlen

## How does alle the data looks like
- Infos und Bilder fehlen

## Glossary
| Topics | Description |
|------|------| 
| PUBLISH | Used in the ATOS CE environment. Send data to an existing channel |
| SUBSCRIBE | Used in the ATOS CE environment. Attach to the "output" of a channel |
| Tour/trip | Used in the PTV ETA environment. - |
| Stop | Used in the PTV ETA environment. - |
| SCEMID | - |
| Subscription | Used in the PTV ETA environment. - |
| Notification | Used in the PTV ETA environment. - |

## History
### Version 2.0.0 - 2.0.n
| version | description |
|------|------| 
| 2.0.0 | Angular 5 |

### Version 1.0.0 - 1.0.n
| version | description |
|------|------| 
| 1.0.5 | Modified this file |
| 1.0.5 | Added a NOTOFICATION channel for ETA and STATUS informations |
| 1.0.4 | Used actual angular/material Beta-12 |
| 1.0.0 | Started to use DAve Subscription(s) |

