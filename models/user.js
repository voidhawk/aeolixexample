var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema(
    {
        email: {type: String, required: true, unique: true},
        password: {type: String, required: false},
        role: { type: 'Object'},
        config: { type: 'Object'}
    }, {
        collection: 'user'
    }
);
schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('UserMgmt', schema);