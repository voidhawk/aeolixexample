var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema(
    {
        publishResp2Url: String,
        publish2Url: String,
        subType: String,
        service: String,
        creationTime: Date,
        uniqueID: String,
        originator: { type: 'Object' },
        data: { type: 'Object', required: true }
    }, {
        collection: 'aeolix'
    }
);

module.exports = mongoose.model('aeolix', schema);