/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Login component implementation

Info:
20171124 | Created

*/
import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatFormField, MAT_DIALOG_DATA } from '@angular/material';


@Component({
    selector: 'login-user-dialog',
    templateUrl: 'login.component.html',
})
export class LoginDlg {

    constructor(public dialogRef: MatDialogRef<LoginDlg>, @Inject(MAT_DIALOG_DATA) public data: any) {

    }

    onNoClick(): void {

        this.dialogRef.close();
    }

    onChangeEMailSel($event) {

        this.data.email = $event.value;
        console.log("Selected eMail: " + $event.value);
    }
}