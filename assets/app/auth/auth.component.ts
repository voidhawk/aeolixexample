/*
Author: Theo Junge, J�rgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Toolbar component implementation

Info:
20171025 | Created

*/
//Test wieder einmal
// Bitbucket Zeile

import { Component, Inject, OnInit } from '@angular/core';

// und von lokal
import { TranslateService } from '@ngx-translate/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { TV_User, TV_Role, TV_Permission, TV_Config, PermittedAction } from 'thevoidmodelmodule/dist/shared';
import { Subscribe, AeonSubscription } from 'thevoidmodelmodule/dist/aeolix';
import { TheVoidDataService } from '../datamgmt/thevoid-data.service';
import { LoginDlg } from './dlgs/login.component'
import { Subscription } from 'rxjs/Subscription';

var uniquid = require('uniquid');

@Component({
    selector: 'auth-header',
    templateUrl: 'auth.component.html',
    styles: [`.test {flex: 1 1 auto;}`],
    styleUrls: ["./auth.component.css"]
})
export class AuthComponent {

    private user: string;
    private password: string;
    private signedin: boolean = false;

    constructor(private dialog: MatDialog, private translate: TranslateService, private theVoidDataService: TheVoidDataService) {

        translate.addLangs(["en", "fr", "de", "gr", "it", "es"]);
        translate.setDefaultLang('en');

        let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr|de|gr|it|es/) ? browserLang : 'en');
        this.user = "jst_aeon_prod@aeolix.eu";
    }

    onOpenAuth() {

        var email;
        let dialogRef = this.dialog.open(LoginDlg, {
            width: '400px',
            //height: '200px',
            data: { email: this.user, password: this.password }
        });

        dialogRef.afterClosed().subscribe(result => {

            console.log('The login dialog was closed');
            this.password = result;
            console.log(result);
            this.checkUser(result.email);

        });
    }

    checkUser(mail: string) {

        if (mail === "aeolix@aeon.eu") {

            var fakeUserAndConfiguration = new TV_User(mail);
            var userLanguage = new TV_Config();
            userLanguage.key = "DefaultLanguage";
            userLanguage.value = "de";
            fakeUserAndConfiguration.configs.push(userLanguage);

            var aeolixRole = new TV_Role();
            aeolixRole.name = "Aeolix";
            var userRoleConfig = new TV_Config();
            userRoleConfig.key = aeolixRole.name;
            userRoleConfig.value = aeolixRole;
            fakeUserAndConfiguration.roles.push(userRoleConfig);

            /**
             * PUBLISH permissions
             */
            var permissionsPublish = new TV_Permission();
            permissionsPublish.permission = "PUBLISH";

            var publish_2_ExtPublish = new TV_Config();
            publish_2_ExtPublish.key = "ExtPublish";
            publish_2_ExtPublish.value = "https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe";
            permissionsPublish.configs.push(publish_2_ExtPublish);

            aeolixRole.permissions.push(permissionsPublish);

            /**
             * SUBSCRIBE permissions
             */
            var permissionsSubscribe = new TV_Permission();
            permissionsSubscribe.permission = "SUBSCRIBE";


            // Channel ExtPublish

            var extPublishSubscription = new Subscribe();
            extPublishSubscription.subscriptionUrl = "https://aeon.atosresearch.eu:3000/subscribe/e993dd47-4b59-4133-a4a6-0e18dcf31d8a";
            extPublishSubscription.subscription = new AeonSubscription();
            extPublishSubscription.subscription._id = "5a200b370fcea200133bdce9";
            extPublishSubscription.subscription.id = "aeolix.aeon@cloud.com";
            extPublishSubscription.subscription.desc = "bhk8mjmxpko0a";
            extPublishSubscription.subscription.ip = "aeon.atosresearch.eu:3000";
            extPublishSubscription.subscription.subkey = "ExtPublish-6234623-queue";


            var subscribe_2_ExtPublish = new TV_Config();
            subscribe_2_ExtPublish.key = "ExtPublish";
            subscribe_2_ExtPublish.value = extPublishSubscription;
            permissionsSubscribe.configs.push(subscribe_2_ExtPublish)



            // Channel ExtSubscribe
            var extSubscribeSubscription = new Subscribe();
            extSubscribeSubscription.subscriptionUrl = "https://aeon.atosresearch.eu:3000/subscribe/4aa2633a-cbeb-43bd-a5ac-85d04f6b038b";
            extSubscribeSubscription.subscription = new AeonSubscription();
            extSubscribeSubscription.subscription._id = "5a1cfd780fcea200133b836e";
            extSubscribeSubscription.subscription.id = "aeolix.aeon@cloud.com";
            extSubscribeSubscription.subscription.desc = "bhi9uxpbqmg0";
            extSubscribeSubscription.subscription.ip = "aeon.atosresearch.eu:3000";
            extSubscribeSubscription.subscription.subkey = "ExtSubscribe-7261204-queue";

            var subscribe_2_ExtSubscribe = new TV_Config();
            subscribe_2_ExtSubscribe.key = "ExtSubscribe";
            subscribe_2_ExtSubscribe.value = extSubscribeSubscription;
            permissionsSubscribe.configs.push(subscribe_2_ExtSubscribe)

            // Channel Notification
            let notificationSubscription = new Subscribe();
            notificationSubscription.subscriptionUrl = "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c";
            notificationSubscription.subscription = new AeonSubscription();
            notificationSubscription.subscription._id = "5a1cfdbe0fcea200133b8388";
            notificationSubscription.subscription.id = "aeolix.aeon@cloud.com";
            notificationSubscription.subscription.desc = "bhi9vvgtfs00";
            notificationSubscription.subscription.ip = "aeon.atosresearch.eu:3000";
            notificationSubscription.subscription.subkey = "ExtSubscribe-67856577-queue";

            var subscribe_2_Notification = new TV_Config();
            subscribe_2_Notification.key = "Notification";
            subscribe_2_Notification.value = notificationSubscription;
            permissionsSubscribe.configs.push(subscribe_2_Notification)

            aeolixRole.permissions.push(permissionsSubscribe);

            /**
             * Database
             */
            var tourCollection = new TV_Config();
            tourCollection.key = "TourMetaData";
            tourCollection.value = "tourmetadata";
            fakeUserAndConfiguration.configs.push(tourCollection);

            /**
             * And now fake a successfull Login
             */
            this.login(fakeUserAndConfiguration);
        }
        else if (mail === "jst_aeon_prod@aeolix.eu") {

            var fakeUserAndConfiguration = new TV_User(mail);
            var userLanguage = new TV_Config();
            userLanguage.key = "DefaultLanguage";
            userLanguage.value = "de";
            fakeUserAndConfiguration.configs.push(userLanguage);

            var aeolixRole = new TV_Role();
            aeolixRole.name = "Aeolix";
            var userRoleConfig = new TV_Config();
            userRoleConfig.key = aeolixRole.name;
            userRoleConfig.value = aeolixRole;
            fakeUserAndConfiguration.roles.push(userRoleConfig);

            /**
             * Configuration of the PUBLISH section(s)
             */
            {
                var permissionsPublish = new TV_Permission();
                permissionsPublish.permission = "PUBLISH";

                // "ExtPublish" channel (production-zone)
                var publish_2_ExtPublish = new TV_Config();
                publish_2_ExtPublish.key = "ExtPublish";
                publish_2_ExtPublish.value = "https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe";
                permissionsPublish.configs.push(publish_2_ExtPublish);

                // "ExtSubscribe" channel (production-zone)
                var publish_2_ExtSubscribe = new TV_Config();
                publish_2_ExtSubscribe.key = "ExtSubscribe";
                publish_2_ExtSubscribe.value = "https://aeon.atosresearch.eu:3000/publish/2ff0d533-a89d-4864-a2d5-776821fcbb0e";
                permissionsPublish.configs.push(publish_2_ExtSubscribe);

                // "Notification" channel (production-zone)
                var publish_2_Notification = new TV_Config();
                publish_2_Notification.key = "Notification";
                publish_2_Notification.value = "https://aeon.atosresearch.eu:3000/publish/0d77bcbe-e7fc-4c69-acc7-b55b08382f1f";
                permissionsPublish.configs.push(publish_2_Notification);

                // add to configuration
                aeolixRole.permissions.push(permissionsPublish);
            }

            /**
             * Configuration of the SUBSCRIBE section(s)
             */
            {
                var permissionsSubscribe = new TV_Permission();
                permissionsSubscribe.permission = "SUBSCRIBE";

                /** 
                 * "ExtPublish" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtPublish",
                    "https://aeon.atosresearch.eu:3000/subscribe/e993dd47-4b59-4133-a4a6-0e18dcf31d8a", mail, "ExtPublish.JSt.Prod",
                    "5a3253e853d1a200131215eb", "aeon.atosresearch.eu:3000", "ExtPublish-81439731-queue", PermittedAction.USE));

                /**
                 * "ExtSubscribe" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtSubscribe",
                    "https://aeon.atosresearch.eu:3000/subscribe/4aa2633a-cbeb-43bd-a5ac-85d04f6b038b", mail, "ExtSubscribe.JSt.Prod",
                    "5a3253e853d1a200131215ed", "aeon.atosresearch.eu:3000", "ExtSubscribe-2603352-queue", PermittedAction.USE));

                /**
                 * "Notification" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Notification",
                    "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c", mail, "Notification.JSt.Prod",
                    "5a3253e853d1a200131215ec", "aeon.atosresearch.eu:3000", "Notification-41892040-queue", PermittedAction.USE));

                /**
                 * Add everything to the role
                 */
                aeolixRole.permissions.push(permissionsSubscribe);
            }

            /**
             * Database
             */
            var tourCollection = new TV_Config();
            tourCollection.key = "TourMetaData";
            tourCollection.value = "tourmetadata";
            fakeUserAndConfiguration.configs.push(tourCollection);

            /**
             * And now fake a successfull Login
             */
            this.login(fakeUserAndConfiguration);
        }
        else if (mail === "jst_aeon_dev@aeolix.eu") {

            var fakeUserAndConfiguration = new TV_User(mail);
            var userLanguage = new TV_Config();
            userLanguage.key = "DefaultLanguage";
            userLanguage.value = "de";
            fakeUserAndConfiguration.configs.push(userLanguage);

            {
                var tourMgmtRole = new TV_Role();
                tourMgmtRole.name = "TourMgmt";
                var tourMgmtRoleConfig = new TV_Config();
                tourMgmtRoleConfig.key = tourMgmtRole.name;
                tourMgmtRoleConfig.value = tourMgmtRole;
                fakeUserAndConfiguration.roles.push(tourMgmtRoleConfig);
            }

            {
                var oneSignalRole = new TV_Role();
                oneSignalRole.name = "OneSignal";
                var oneSignalRoleConfig = new TV_Config();
                oneSignalRoleConfig.key = oneSignalRole.name;
                oneSignalRoleConfig.value = oneSignalRole;
                fakeUserAndConfiguration.roles.push(oneSignalRoleConfig);
            }

            var aeolixRole = new TV_Role();
            aeolixRole.name = "Aeolix";
            var userRoleConfig = new TV_Config();
            userRoleConfig.key = aeolixRole.name;
            userRoleConfig.value = aeolixRole;
            fakeUserAndConfiguration.roles.push(userRoleConfig);

            /**
             * Configuration of the PUBLISH section
             */
            {
                var permissionsPublish = new TV_Permission();
                permissionsPublish.permission = "PUBLISH";

                // "ExtPublish" channel (production-zone)
                var publish_2_ExtPublish = new TV_Config();
                publish_2_ExtPublish.key = "ExtPublish";
                publish_2_ExtPublish.value = "https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe";
                permissionsPublish.configs.push(publish_2_ExtPublish);

                // "ExtSubscribe" channel (production-zone)
                var publish_2_ExtSubscribe = new TV_Config();
                publish_2_ExtSubscribe.key = "ExtSubscribe";
                publish_2_ExtSubscribe.value = "https://aeon.atosresearch.eu:3000/publish/2ff0d533-a89d-4864-a2d5-776821fcbb0e";
                permissionsPublish.configs.push(publish_2_ExtSubscribe);

                // "Notification" channel (production-zone)
                var publish_2_Notification = new TV_Config();
                publish_2_Notification.key = "Notification";
                publish_2_Notification.value = "https://aeon.atosresearch.eu:3000/publish/0d77bcbe-e7fc-4c69-acc7-b55b08382f1f";
                permissionsPublish.configs.push(publish_2_Notification);

                // "Publish" channel (dev-zone)
                var publish_2_Publish = new TV_Config();
                publish_2_Publish.key = "Publish";
                publish_2_Publish.value = "https://aeon-dev.atosresearch.eu:3000/publish/a8732d90-8670-46f9-8822-7acac6c7c4e6";
                permissionsPublish.configs.push(publish_2_Publish);

                // "Receive" channel (dev-zone)
                var publish_2_Receive = new TV_Config();
                publish_2_Receive.key = "Receive";
                publish_2_Receive.value = "https://aeon-dev.atosresearch.eu:3000/publish/9046dbe8-f19f-4f85-b8bb-6726fc1e1cb5";
                permissionsPublish.configs.push(publish_2_Receive);

                // "Notification" channel (dev-zone)
                var publish_2_Notification = new TV_Config();
                publish_2_Notification.key = "Notification";
                publish_2_Notification.value = "https://aeon-dev.atosresearch.eu:3000/publish/d7c1685e-0cbf-41f4-81d7-9f4cc865639f";
                permissionsPublish.configs.push(publish_2_Notification);

                // add to configuration
                aeolixRole.permissions.push(permissionsPublish);
            }

            /**
             * Configuration of the SUBSCRIBE section
             */
            {
                var permissionsSubscribe = new TV_Permission();
                permissionsSubscribe.permission = "SUBSCRIBE";

                /** 
                 * "ExtPublish" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtPublish",
                    "https://aeon.atosresearch.eu:3000/subscribe/e993dd47-4b59-4133-a4a6-0e18dcf31d8a", mail, "ExtPublish.JSt.Prod",
                    "5a3253e853d1a200131215eb", "aeon.atosresearch.eu:3000", "ExtPublish-81439731-queue", PermittedAction.USE));

                /**
                 * "ExtSubscribe" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtSubscribe",
                    "https://aeon.atosresearch.eu:3000/subscribe/4aa2633a-cbeb-43bd-a5ac-85d04f6b038b", mail, "ExtSubscribe.JSt.Prod",
                    "5a3253e853d1a200131215ed", "aeon.atosresearch.eu:3000", "ExtSubscribe-2603352-queue", PermittedAction.USE));

                /**
                 * "Notification" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Notification",
                    "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c", mail, "Notification.JSt.Prod",
                    "5a3253e853d1a200131215ec", "aeon.atosresearch.eu:3000", "Notification-41892040-queue", PermittedAction.USE));

                /** 
                 * "Publish" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Publish",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/e3fe1013-c32d-4ef4-b2f4-fc865edca050", mail, "Publish.JSt.Dev",
                    "5a3253e95a0aee00111a4298", "aeon-dev.atosresearch.eu:3000", "Publish-63050149-queue", PermittedAction.NONE));

                /** 
                 * "Receive" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Receive",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/d37719a9-e50f-478a-a224-d86dc4e67220", mail, "Receive.JSt.Dev",
                    "5a3253e95a0aee00111a4299", "aeon-dev.atosresearch.eu:3000", "Receive-19462541-queue", PermittedAction.NONE));

                /** 
                 * "Notification" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Notification",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/fdac5085-bff0-4396-a9a7-27cd55bfc36d", mail, "Notification.JSt.Dev",
                    "5a3253e95a0aee00111a429a", "aeon-dev.atosresearch.eu:3000", "Notification-33032397-queue", PermittedAction.NONE));

                /**
                 * Add everything to the role
                 */
                aeolixRole.permissions.push(permissionsSubscribe);
            }

            /**
             * Database
             */
            var tourCollection = new TV_Config();
            tourCollection.key = "TourMetaData";
            tourCollection.value = "tourmetadata";
            fakeUserAndConfiguration.configs.push(tourCollection);

            /**
             * And now fake a successfull Login
             */
            this.login(fakeUserAndConfiguration);
        }
        else if (mail === "thej_aeon_dev@aeolix.eu") {

            var fakeUserAndConfiguration = new TV_User(mail);
            var userLanguage = new TV_Config();
            userLanguage.key = "DefaultLanguage";
            userLanguage.value = "de";
            fakeUserAndConfiguration.configs.push(userLanguage);

            var aeolixRole = new TV_Role();
            aeolixRole.name = "Aeolix";
            var userRoleConfig = new TV_Config();
            userRoleConfig.key = aeolixRole.name;
            userRoleConfig.value = aeolixRole;
            fakeUserAndConfiguration.roles.push(userRoleConfig);

            /**
             * Configuration of the PUBLISH section
             */
            {
                var permissionsPublish = new TV_Permission();
                permissionsPublish.permission = "PUBLISH";

                // "ExtPublish" channel (production-zone)
                var publish_2_ExtPublish = new TV_Config();
                publish_2_ExtPublish.key = "ExtPublish";
                publish_2_ExtPublish.value = "https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe";
                permissionsPublish.configs.push(publish_2_ExtPublish);

                // "ExtSubscribe" channel (production-zone)
                var publish_2_ExtSubscribe = new TV_Config();
                publish_2_ExtSubscribe.key = "ExtSubscribe";
                publish_2_ExtSubscribe.value = "https://aeon.atosresearch.eu:3000/publish/2ff0d533-a89d-4864-a2d5-776821fcbb0e";
                permissionsPublish.configs.push(publish_2_ExtSubscribe);

                // "Notification" channel (production-zone)
                var publish_2_Notification = new TV_Config();
                publish_2_Notification.key = "Notification";
                publish_2_Notification.value = "https://aeon.atosresearch.eu:3000/publish/0d77bcbe-e7fc-4c69-acc7-b55b08382f1f";
                permissionsPublish.configs.push(publish_2_Notification);

                // "Publish" channel (dev-zone)
                var publish_2_Publish = new TV_Config();
                publish_2_Publish.key = "Publish";
                publish_2_Publish.value = "https://aeon-dev.atosresearch.eu:3000/publish/a8732d90-8670-46f9-8822-7acac6c7c4e6";
                permissionsPublish.configs.push(publish_2_Publish);

                // "Receive" channel (dev-zone)
                var publish_2_Receive = new TV_Config();
                publish_2_Receive.key = "Receive";
                publish_2_Receive.value = "https://aeon-dev.atosresearch.eu:3000/publish/9046dbe8-f19f-4f85-b8bb-6726fc1e1cb5";
                permissionsPublish.configs.push(publish_2_Receive);

                // "Notification" channel (dev-zone)
                var publish_2_Notification = new TV_Config();
                publish_2_Notification.key = "Notification";
                publish_2_Notification.value = "https://aeon-dev.atosresearch.eu:3000/publish/d7c1685e-0cbf-41f4-81d7-9f4cc865639f";
                permissionsPublish.configs.push(publish_2_Notification);

                // add to configuration
                aeolixRole.permissions.push(permissionsPublish);
            }

            /**
             * Configuration of the SUBSCRIBE section
             */
            {
                var permissionsSubscribe = new TV_Permission();
                permissionsSubscribe.permission = "SUBSCRIBE";

                /** 
                 * "ExtPublish" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtPublish",
                    "https://aeon.atosresearch.eu:3000/subscribe/e993dd47-4b59-4133-a4a6-0e18dcf31d8a", mail, "ExtPublish.Thej.Prod",
                    "5a3253e853d1a200131215eb", "aeon.atosresearch.eu:3000", "ExtPublish-81439731-queue", PermittedAction.USE));

                /**
                 * "ExtSubscribe" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("ExtSubscribe",
                    "https://aeon.atosresearch.eu:3000/subscribe/4aa2633a-cbeb-43bd-a5ac-85d04f6b038b", mail, "ExtSubscribe.Thej.Prod",
                    "5a3253e853d1a200131215ed", "aeon.atosresearch.eu:3000", "ExtSubscribe-2603352-queue", PermittedAction.USE));

                /**
                 * "Notification" channel (production-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Notification",
                    "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c", mail, "Notification.Thej.Prod",
                    "5a3253e853d1a200131215ec", "aeon.atosresearch.eu:3000", "Notification-41892040-queue", PermittedAction.USE));

                /** 
                 * "Publish" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Publish",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/e3fe1013-c32d-4ef4-b2f4-fc865edca050", mail, "Publish.Thej.Dev",
                    "5a3253e95a0aee00111a4298", "aeon-dev.atosresearch.eu:3000", "Publish-63050149-queue", PermittedAction.NONE));

                /** 
                 * "Receive" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Receive",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/d37719a9-e50f-478a-a224-d86dc4e67220", mail, "Receive.Thej.Dev",
                    "5a3253e95a0aee00111a4299", "aeon-dev.atosresearch.eu:3000", "Receive-19462541-queue", PermittedAction.NONE));

                /** 
                 * "Notification" channel (dev-zone)
                 */
                permissionsSubscribe.configs.push(this.create_SUBSCRIBE_Config("Notification",
                    "https://aeon-dev.atosresearch.eu:3000/subscribe/fdac5085-bff0-4396-a9a7-27cd55bfc36d", mail, "Notification.Thej.Dev",
                    "5a3253e95a0aee00111a429a", "aeon-dev.atosresearch.eu:3000", "Notification-33032397-queue", PermittedAction.NONE));

                /**
                 * Add everything to the role
                 */
                aeolixRole.permissions.push(permissionsSubscribe);
            }


            /**
             * Database
             */
            var tourCollection = new TV_Config();
            tourCollection.key = "TourMetaData";
            tourCollection.value = "tourmetadata";
            fakeUserAndConfiguration.configs.push(tourCollection);

            /**
             * And now fake a successfull Login
             */
            this.login(fakeUserAndConfiguration);
        }
        return;
    }

    onLogOut() {

        console.log("function_logout");
    }

    onAuth($event) {

        //var mail = "jst_aeon_dev@aeolix.eu";
        //var mail = "aeolix@aeolix.eu";

        /**
         * Bisher existierende AEON Channels
         * ****Channel configurations (production zone)****
         * Channel "ExtPublish" (production zone)
         * PUBLISH=> 'https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe';
         * SUBSCRIBE=> 'https://aeon.atosresearch.eu:3000/subscribe/e993dd47-4b59-4133-a4a6-0e18dcf31d8a';
         * 
         * Channel "ExtSubscribe" (production zone)
         *  PUBLISH=> 'https://aeon.atosresearch.eu:3000/publish/2ff0d533-a89d-4864-a2d5-776821fcbb0e';
         *  SUBSCRIBE=> 'https://aeon.atosresearch.eu:3000/subscribe/4aa2633a-cbeb-43bd-a5ac-85d04f6b038b';
         * 
         * Channel "Notification" (production zone)
         *  PUBLISH=> "https://aeon.atosresearch.eu:3000/publish/0d77bcbe-e7fc-4c69-acc7-b55b08382f1f";
         *  SUBSCRIBE=> "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c";

         * ****Channel configurations (dev zone)****
         * Channel "Publish" (dev zone)
         *  PUBLISH=> https://aeon-dev.atosresearch.eu:3000/publish/a8732d90-8670-46f9-8822-7acac6c7c4e6
         *  SUBSCRIBE=> https://aeon-dev.atosresearch.eu:3000/subscribe/e3fe1013-c32d-4ef4-b2f4-fc865edca050

         * Channel "Receive" (dev zone)
         *  PUBLISH=> https://aeon-dev.atosresearch.eu:3000/publish/9046dbe8-f19f-4f85-b8bb-6726fc1e1cb5
         *  SUBSCRIBE=> https://aeon-dev.atosresearch.eu:3000/subscribe/d37719a9-e50f-478a-a224-d86dc4e67220
         * 
         * Channel "Notification" (dev-zone)
         *  PUBLISH=> https://aeon-dev.atosresearch.eu:3000/publish/d7c1685e-0cbf-41f4-81d7-9f4cc865639f
         *  SUBSCRIBE=> https://aeon-dev.atosresearch.eu:3000/subscribe/fdac5085-bff0-4396-a9a7-27cd55bfc36d
         */
        if (this.signedin == true) {

            this.theVoidDataService.setCurrentUser(null);
            this.translate.use('en');
            this.signedin = !this.signedin;
        } else {

            this.onOpenAuth();
            this.signedin = !this.signedin;
        }
    }

    create_SUBSCRIBE_Config(key: string, url: string, mail: string, desc: string, _id: string, ip: string, subkey: string, action: PermittedAction) {

        var subscribeSubscription = new Subscribe();
        subscribeSubscription.subscriptionUrl = url;
        subscribeSubscription.subscription = new AeonSubscription();
        subscribeSubscription.subscription.id = mail;
        subscribeSubscription.subscription.desc = uniquid();
        /*
        subscribeSubscription.subscription.desc = desc

        subscribeSubscription.subscription._id = _id;
        subscribeSubscription.subscription.ip = ip;
        subscribeSubscription.subscription.subkey = subkey;
        */

        var subscribe_TV_Config = new TV_Config();
        subscribe_TV_Config.key = key;
        subscribe_TV_Config.value = subscribeSubscription;
        if (action != null) {

            subscribe_TV_Config.action = action;
        }

        return subscribe_TV_Config;
    }
    /**
     * Try to login the requested user. If successfull update the TheVoidDataService.
     * 
     * @param user 
     */
    login(user: TV_User) {

        this.theVoidDataService.setCurrentUser(user);
        var language = this.theVoidDataService.getConfigOfUser("DefaultLanguage");
        this.translate.use(language);
    }

    openDlgShowMessages(target: string, $event) {
        //ToDo
        console.log('openDLG called.');
    }
}


