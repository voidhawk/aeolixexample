/*
Author: Theo Junge, Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
This class contains all module dependent information for the 
Toolbar implementation.

Info:
20171025 | Created

*/
import { NgModule } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { AuthComponent } from "./auth.component";
import {
    MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule, MatToolbarModule, MatIconModule, MatTooltipModule,
    MatListModule
} from '@angular/material';
import {
    MatCheckboxModule, MatSelectModule,
    MatSidenavModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule, MatPaginatorModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginDlg } from './dlgs/login.component'

/*
Translation section
*/
import { HttpModule, Http } from "@angular/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from '@angular/common/http';
export function HttpLoaderFactory(http: HttpClient) {

    return new TranslateHttpLoader(http, "./i18n/", ".json");
}

@NgModule({
    declarations: [AuthComponent, LoginDlg],
    imports: [
        BrowserModule,
        MatButtonModule,
        MatTooltipModule,
        MatSelectModule,
        MatIconModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatListModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatPaginatorModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [

    ],
    entryComponents: [
        LoginDlg
    ]
})

export class AuthModule {

    /*    constructor(mdIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
            mdIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./svg/mdi.svg')); // Or whatever path you placed mdi.svg at
        }
    */
}