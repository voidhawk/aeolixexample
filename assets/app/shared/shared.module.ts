/*
Author: Theo Junge / PTV Group
Copyright: 2017
Research project Aeolix

Description: 

Info:
20171207 | Created

*/
import { NgModule } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import {
    MatInputModule, MatFormFieldModule, MatButtonModule, MatDialogModule, MatToolbarModule, MatIconModule, MatTooltipModule,
    MatListModule, MatAutocomplete
} from '@angular/material';

import {
    MatCheckboxModule, MatSelectModule,
    MatSidenavModule, MatExpansionModule, MatDatepickerModule, MatNativeDateModule, MatPaginatorModule, MatOptionModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OneSignalComponent } from "./dlgs/onesignal/onesignal.component";

/*
Translation section
*/
import { HttpModule, Http } from "@angular/http"; // => brauchen wir das (Http) noch????
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
export function HttpLoaderFactory(http: HttpClient) {

    return new TranslateHttpLoader(http, "./i18n/", ".json");
}

@NgModule({
    declarations: [OneSignalComponent],
    imports: [
        BrowserModule,
        MatButtonModule,
        MatTooltipModule,
        MatSelectModule,
        MatIconModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatListModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatPaginatorModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        FormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [

    ],
    entryComponents: [
        OneSignalComponent
    ]
})

export class SharedModule {

}