/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Login component implementation

Info:
20171207 | Created

*/
import { Component, Inject } from '@angular/core';
import { RequestOptions, ResponseContentType, } from '@angular/http';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import { MatDialog, MatDialogRef, MatFormField, MatSelectChange, MatOption, MatInput,
    MatAutocomplete,
    MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'onesignal-component',
    templateUrl: 'onesignal.component.html',
})
export class OneSignalComponent {

    private oneSignalAppIds: IComboBoxItem[] = [];
    private oneSignalAppId: IComboBoxItem;
    private oneSignalPlayerIds: IComboBoxItem[] = [];
    private oneSignalPlayerId: IComboBoxItem;
    private oneSignalMessageIds: IComboBoxItem[] = [];
    private oneSignalMessageId: IComboBoxItem;
    private oneSignalData: any;

    private showText2Read: boolean;
    private text2Read: string;

    constructor(private http: HttpClient, private dialogRef: MatDialogRef<OneSignalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {

        this.oneSignalAppId = {

            value: 'be00e13a-c405-4f93-81e2-6451e91b89f6',
            viewValue: 'DAve'
        }
        /**
         * ComboBox content for OneSignal App_Ids
         * 
         * ID von Devid stimmt (noch) nicht
         */
        var onesignal_ids = [
            { viewValue: 'DAve', value: 'be00e13a-c405-4f93-81e2-6451e91b89f6' },
            { viewValue: 'DVo', value: 'be00e13a-c405-4f93-81e2-6451e91b89f6' }
        ];
        this.oneSignalAppIds.push(onesignal_ids[0]); //Redmi
        this.oneSignalAppIds.push(onesignal_ids[1]); // Nexus DVo

        this.oneSignalPlayerId = {

            value: '19dd97f0-e161-4dbb-bbf3-6e66330be45a',
            viewValue: 'JSt'
        }
        /**
         * ComboBox content for OneSignal Player_Ids
         */
        var player_ids = [
            //{ viewValue: 'JSt', value: 'a115d8f2-9a3c-4683-b09e-6a7eabd8649d' },
            { viewValue: 'JSt', value: '0753879e-6f36-4a60-8668-abced6f314cc' },
            { viewValue: 'DVo', value: '19dd97f0-e161-4dbb-bbf3-6e66330be45a' },
            { viewValue: 'Nexus', value: 'a1202526-606c-44ab-b60e-fab54a5c2262' }
        ];
        this.oneSignalPlayerIds.push(player_ids[0]); // JSt
        this.oneSignalPlayerIds.push(player_ids[1]); // DVo
        this.oneSignalPlayerIds.push(player_ids[2]); // Nexus

        //dvo
        var message_ids = [
            { viewValue: 'Neue Tour', value: '0' },
            { viewValue: 'Tour Update', value: '1' },
            { viewValue: 'Geofence arrived', value: '2' },
            { viewValue: 'Geofence departed', value: '3' },
            { viewValue: 'Message inform', value: '4' },
            { viewValue: 'Config Location Update', value: '5' }
        ];
        this.oneSignalMessageIds.push(message_ids[0]);
        this.oneSignalMessageIds.push(message_ids[1]);
        this.oneSignalMessageIds.push(message_ids[2]);
        this.oneSignalMessageIds.push(message_ids[3]);
        this.oneSignalMessageIds.push(message_ids[4]);
        this.oneSignalMessageIds.push(message_ids[5]);

        this.text2Read = "Bitte beim Disponenten anrufen.";
        this.showText2Read = true;
    }

    /**
     * Used to store the last settings of the dialog.
     */
    onSaveData(): void {

        localStorage.setItem('OneSignalAppId', this.oneSignalAppId.value);
        localStorage.setItem('OneSignalPlayerId', this.oneSignalPlayerId.value);
        this.dialogRef.close();
    }

    onNoClick(): void {

        this.dialogRef.close();
    }

    onSendData(): void {

        let headers = new HttpHeaders({ 'Content-type': 'application/json' });
        var options = {

            headers: headers
        };

        var data = this.getCreateMsg();

        var tempPlayerIDs: string[] = [];
        tempPlayerIDs.push(this.oneSignalPlayerId.value);
        var body = {

            app_id: this.oneSignalAppId.value,
            data: data,
            contents: { "en": "English Message" },
            include_player_ids: tempPlayerIDs
        };

        const oneSignalPOST = this.http.post('https://onesignal.com/api/v1/notifications', body, options).subscribe(
            res => {
                console.log(res);
            },
            err => {
                console.log("Error occured");
            }
        );
    }


    getCreateMsg(): any {

        switch (parseInt(this.oneSignalMessageId.value)) {

            case 0:
                var data = {
                    'type': 'TOUR',
                    'action': 'NEW',
                    'content': '351Q2ZJHP6'
                };
                return data;

            case 1:
                var data = {
                    'type': 'TOUR',
                    'action': 'UPDATE',
                    'content': ''
                };
                return data;

            case 2:
                var data = {
                    'type': 'STOP',
                    'action': 'GEOFENCE_ARRIVED',
                    'content': ''
                };
                return data;

            case 3:
                var data = {
                    'type': 'STOP',
                    'action': 'GEOFENCE_DEPARTED',
                    'content': ''
                };
                return data;

            case 4:
                var data = {
                    'type': 'MESSAGE',
                    'action': 'INFORM',
                    'content': 'Bitte beim Disponenten anrufen.'
                };
                this.showText2Read = false;
                return data;

            case 5:
                var data = {
                    'type': 'CONFIG',
                    'action': 'LOCATION_UPDATE',
                    'content': '5'
                };
                return data;
        }
    }
}

export interface IComboBoxItem {

    'value': string;
    'viewValue': string;
}

export interface IOneSignalData {

    'type': OS_TYPE;
    'action': OS_ACTION;
    'content'?: any;
}

export enum OS_TYPE {

    UNDEFINED = 0,
    TOUR = 1,
    STOP = 2,
    TEXT = 3
}

export enum OS_ACTION {

    UNDEFINED = 0,
    NEW = 1,
    READ = 2,
    UPDATE = 3,
    DELETE = 4,
    SPEEK = 5
}