/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20171219 | Created

*/
//import { NO_ERRORS_SCHEMA } from '@angular/core'; // wofür braucht man das eigentlich?
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*
Translation section
*/
import { HttpModule, Http } from "@angular/http";
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {

    return new TranslateHttpLoader(http, "./i18n/", ".json");
}
//import { MongoDBService } from './../datamgmt/mongodb.service';
//import { AeolixComponent } from './aeolix.component';

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ],
    declarations: [
        //AeolixComponent
    ],
    providers: [
        //MongoDBService
    ],
    entryComponents: [
        //ToolsDlg
    ],
    schemas: [
        //NO_ERRORS_SCHEMA
    ]
})
export class TourMgmtModule {

}
