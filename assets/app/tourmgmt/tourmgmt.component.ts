/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20171219 | Created

*/
import { Component, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Component({
    selector: 'scheduler',
    //templateUrl: "./tourmgmt.component.html",
    template: '<div id="scheduler"></div>'
    //styleUrls: ["./tourmgmt.component.css"]
})
export class TourMgmtComponent implements AfterContentInit {

    constructor(private httpClient: HttpClient){

    }
    
    ngAfterContentInit() {
        
    }
}
