/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20170719 | Created

*/
import { Injectable, EventEmitter } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx'; //um .map() benutzen zu können
import { Observable } from "rxjs";

import { LocationType, TheVoidLocation } from 'thevoidmodelmodule/dist/shared';

@Injectable()
export class DAveService {

    locationIsEdit = new EventEmitter<Location>();
    locationModalCall = new EventEmitter<Location>();

    private locations: TheVoidLocation[] = [];

    constructor(private http: Http) {

    }

    /**
     * 
     * @param currentDAveToken 
     */
    getLocationsOfToken(url: string, currentDAveToken: string, sortOrder: string, 
        itemsPerPage: number, startIndex: number, source: string) {

        const appSource = "TheAppSource";
        //const currentDAveToken = localStorage.getItem('currentDAveToken')
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const getRequest = 'https://driveandarrive-v1.cloud.ptvgroup.com:443/em/location?SortOrder=etaInfo.eta|DESC&ItemsPerPage=12&StartIndex=0&Token=' + currentDAveToken + '&Source=' + appSource;

        return this.http.get(getRequest, { headers: headers })
            .map((response: Response) => {

                const daveResponse = response.json();
                console.log(daveResponse);
                const daveLocations = daveResponse.locations;
                console.log(daveLocations);

                let transformedLocations: TheVoidLocation[] = [];
                for (let location of daveLocations) {

                    var transformedLocation = new TheVoidLocation(null);
                    transformedLocation.createLocation(LocationType.DAve, location);
                    transformedLocations.push(transformedLocation);
                }

                this.locations = transformedLocations;
                return this.locations;
            })
            .catch((error: Response) => {
                return Observable.throw(error.json());
            });
    }
}
