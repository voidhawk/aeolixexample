/*
Author: Theo Junge / PTV Group
Copyright: 2017
Service to store Data in MongoDB

Description:

Info:
20171116 | Created
*/

import { Injectable, EventEmitter } from "@angular/core";
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';


@Injectable()
export class MongoDBService {

    constructor(private http: Http) {
        interface contentTemplate {
            message: string,
            obj: Object[],
        }
    }
    /*
    getContentByUniqueID<contentTemplate>(NodeServerUrl: string, target: string ){
        return this.http.get(NodeServerUrl + target)
                        .map((response: Response) => response.json().obj)
                        .catch((error: Response) => Observable.throw(error.json()));
    }

    getUniqueIDs<contentTemplate>(NodeServerUrl: string) {
        return this.http.get(NodeServerUrl)       //TODO: target in GET einfügen, damit nur die Daten geholt werden, die man haben will, evtl umstellen auf POST- Request, der den Server verschiedene Get-Requests auslösen lässt.
                        .map((response: Response) => response.json().obj)
                        .catch((error: Response) => Observable.throw(error.json()));  
    }
*/

/*
/
/This function gets the number
/
*/

/*
/
/This function gets the IDs of the Tours, Pos Events, Notifications, etc. 
/
*/
    getOverviewByID<contentTemplate>(NodeServerUrl: string, targetSubType: string, targetUniqueID?: string, targetSCEMID?: string, targetSource? : string){ 
               //Selection with a specific timeframe will be added
        return this.http.get(NodeServerUrl+ '/selection?targetSubType=' + targetSubType + '&targetUniqueID='  + targetUniqueID + '&targetSCEMID=' + targetSCEMID + '&targetSource=' + targetSource)
        .map((response: Response) => response.json().obj)
        .catch((error: Response) => Observable.throw(error.json()));
    }
/*
/
/This function sends the GET-Request to get a Request and the cohesive Response using a certain uniqueID  
/
*/
    getContentToShow<contentTemplate>(NodeServerUrl: string, targetSubType: string, targetUniqueID?: string, targetSCEMID?: string){               //
        return this.http.get(NodeServerUrl+ '?targetSubType=' + targetSubType + '&targetUniqueID='  + targetUniqueID + '&targetSCEMID=' + targetSCEMID)
        .map((response: Response) => response.json().obj)
        .catch((error: Response) => Observable.throw(error.json()));
    }
/*
    getContentByDate<contentTemplate>(NodeServerUrl: string, Date_from: string, Date_until: string){
        return this.http.get(NodeServerUrl+ '/' )
        .map((response: Response) => response.json().obj)                                   //ToDo
        .catch((error: Response) => Observable.throw(error.json()));
    }
*/    
    postContent(message: any, NodeServerUrl: string) {
        
        const headers = new Headers({ 'Content-type': 'application/json' });

        return this.http.post(NodeServerUrl, message, { headers: headers })
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error.json()));
    }

    deleteContent(message: any) {

    }
}