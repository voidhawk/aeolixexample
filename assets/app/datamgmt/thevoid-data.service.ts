/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Service for all needed data

Description:

Info:
20170516 | Created
*/

import { Injectable, EventEmitter } from "@angular/core";

import { Subject } from 'rxjs/Subject';

import { TV_User, TV_Role, TV_Permission, TV_Config } from 'thevoidmodelmodule/dist/shared';
import { MongoDBService } from './../datamgmt/mongodb.service';

//var uniquid = require('uniquid');

@Injectable()
export class TheVoidDataService {

    private user: TV_User

    /**
     * This part depends to user MESSAGING (Login/Logout)
     */
    private userChanged = new Subject<TV_User>()
    userChangedStream$ = this.userChanged.asObservable();

    constructor(private mongoDB: MongoDBService) {

    }

    /**
     * 
     * @param user
     */
    setCurrentUser(user: TV_User) {

        this.user = user;
        console.log(JSON.stringify(user));

        this.mongoDB.postContent(user, 'http://localhost:3000/auth').subscribe(
            data => console.log(data),
            error => console.error(error)
        );

        this.userChanged.next(user);
    }

    getEMailAddress() {

        return this.user.getEMail();
    }

    getConfigOfUser(name: string) {

        var retVal = this.user.getConfigOfUser(name);
        if(retVal === "undefined") {

            return "en";
        }

        return retVal;
        /*
        for (var item of this.user.configs) {
            if (item.key === "DefaultLanguage") {

                console.log("getConfigOfUser(..)" + JSON.stringify(item));
                return item.value;
            }
        }
        */
    }

    getRolePermissionOfUser(role: string, permission: string, name?: string) : any {

        for (var userRole of this.user.roles) {

            if (userRole.key === role) {
                let tvRole: TV_Role;
                tvRole = userRole.value;
                for (var userPermissionItem of tvRole.permissions) {

                    if (userPermissionItem.permission === permission) {

                        if (name === null || name === undefined) {

                            return userPermissionItem;
                        }
                        else {

                            for (var userConfigItem of userPermissionItem.configs) {

                                if (userConfigItem.key === name) {

                                    return userConfigItem;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}