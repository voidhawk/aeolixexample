/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20170711 | Created

*/
import { Routes, RouterModule } from "@angular/router";

/*
import our own components
*/
import { AeolixComponent } from './aeolix/aeolix.component'

const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/aeolix', pathMatch: 'full' },
    { path: 'aeolix', component: AeolixComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);