import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { MongoDBService } from './../../datamgmt/mongodb.service';
import { FormControl, ReactiveFormsModule, Validators, FormGroup } from '@angular/forms';
import { MatButton, MatDatepicker, MatFormField, MatTable, MatTableDataSource, MatDateFormats, MatPaginator, MatNativeDateModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { Component, OnInit, ViewChild, Inject, AfterViewInit } from "@angular/core";
/*
*
*   Dialog to Select a specific Tour
*
*/

@Component({
    selector: 'alxdlgSelect',
    styleUrls: ['../aeolix.component.css'],
    templateUrl: 'aeolixselect.component.html'
})

export class AeolixDlgSelect implements OnInit, AfterViewInit {
    private source;
    private scemids: string[] = [];
    private TourSelected: boolean;
    private dataFetched: boolean;
    AeolixNodeUrl: string;
    FilterForm: FormGroup;
    displayedColumns = ['TourSCEMID', 'StopSCEMIDS', 'NumPosEvent', 'NumSubscription', 'NumETA'];
    dataSource = new MatTableDataSource(this.scemids);
    constructor(private mongoDB: MongoDBService, public dialogRef: MatDialogRef<AeolixDlgSelect>, @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }
    ngOnInit() {
        this.source = localStorage.getItem('AeolixSource');
        this.dataFetched = false;
        this.TourSelected = false;
        this.AeolixNodeUrl = 'http://localhost:3000/aeolix_server';
        this.FilterForm = new FormGroup({
            uniqueId: new FormControl(null),
            Date_from: new FormControl(null),
            Date_until: new FormControl(null)
        });

    }

    onSubmit() {
        console.log(this.FilterForm);
        if (this.FilterForm.value.uniqueId == null && Date.parse(this.FilterForm.value.Date_from) < Date.parse(this.FilterForm.value.Date_until)) {
            console.log('Calling a specific Time not implemented yet');
            return null; //Suche über Datum
        } else if (this.FilterForm.value.uniqueId == null && Date.parse(this.FilterForm.value.Date_from) > Date.parse(this.FilterForm.value.Date_until)) {
            console.log('Calling a specific Time not implemented yet, although the dates need to be swapped');
            return null;
        }
        console.log('Getting the request to a specific uniqueId');
        return this.mongoDB.getOverviewByID(this.AeolixNodeUrl, 'Tour', this.FilterForm.value.uniqueId, undefined, this.source)
            .subscribe(
            content => {
                //if (this.scemids != [] || this.scemids == undefined) {this.scemids = [];}

                for (var i = 0; i < content.length; i++) {
                    this.scemids.push(content[i]);
                    
                }
                this.dataSource._updateChangeSubscription();
                this.dataSource._updatePaginator(content.length);
                this.dataFetched = true;

            },
            error => console.error(error)
            );
    }

    onDelete(tour: any) {
        console.log(tour);
    }
    arraysum(a: number[]) {
        return a.reduce(function (a, b) {
            return a + b;
        })
    }

    onTourSelected(row: TourInformation) {
        console.log(row);
        console.log(row.uniqueID);
        console.log(this.TourSelected);
        this.TourSelected = true;
        console.log(this.TourSelected);
        this.data.uniqueID = row.uniqueID;

    }
}



/*
*
*   Dialog to Select a specific Position Event
*
*/

@Component({
    selector: 'posEventdlg',
    styleUrls: ['../aeolix.component.css'],
    templateUrl: 'poseventselect.component.html'
})
export class PosEventSelectDlg {
    AeolixNodeUrl: string;
    displayedColumns = ['StopSCEMID'];
    dataSource = new MatTableDataSource(this.data.scemids);
    constructor(private mongoDB: MongoDBService, public dialogRef: MatDialogRef<PosEventSelectDlg>, @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }
    onPosEventSelected(row) {
        console.log(row);
        this.data.scemid = row;
    }
}

/*
*
*   Dialog to Select a specific Notification
*
*/

@Component({
    selector: 'notificationdlg',
    styleUrls: ['../aeolix.component.css'],
    templateUrl: 'notificationselect.component.html'
})
export class NotificationSelectDlg {
    AeolixNodeUrl: string;
    displayedColumns = ['StopSCEMID'];
    dataSource = new MatTableDataSource(this.data.scemids);
    constructor(private mongoDB: MongoDBService, public dialogRef: MatDialogRef<NotificationSelectDlg>, @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }
    onNotificationSelected(row) {
        console.log(row);
        this.data.scemid = row;
    }
}

/*
*
*   Dialog to Select a specific Subscription
*
*/

@Component({
    selector: 'subscriptiondlg',
    styleUrls: ['../aeolix.component.css'],
    templateUrl: 'subscriptionselect.component.html'
})
export class SubscriptionSelectDlg {
    AeolixNodeUrl: string;
    displayedColumns = ['StopSCEMID'];
    dataSource = new MatTableDataSource(this.data.scemids);
    constructor(private mongoDB: MongoDBService, public dialogRef: MatDialogRef<SubscriptionSelectDlg>, @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }
    onSubscriptionSelected(row) {
        console.log(row);
        this.data.scemid = row;
    }
}
export interface TourInformation {
    uniqueID: string;
    TourSCEMID: string;
    StopSCEMIDS: string[];
    NumPosEvent: number[];
    NumSubscription: number[];
    NumETA: number[];
    Options: any;
}


const Tour_Example: TourInformation[] = [
    {
        uniqueID: 'bhe5pg62i280',
        TourSCEMID: 'ABCDEFGHIJ',
        StopSCEMIDS: ['QWERTZU', 'ASDFSDFGJ', 'YXCVBN', 'WREEZUUI', 'RETUKSFGHJ'],
        NumPosEvent: [3, 6, 5, 7, 8],
        NumSubscription: [12, 22, 45, 65, 70],
        NumETA: [123, 234, 345, 456, 567],
        Options: 0
    },
    {
        uniqueID: 'asoi2392345',
        TourSCEMID: 'qwerwrezrtez',
        StopSCEMIDS: ['QWERTZU', 'ASDFSDFGJ', 'YXCVBN', 'WREEZUUI', 'RETUKSFGHJ'],
        NumPosEvent: [3, 6, 5, 7, 8],
        NumSubscription: [12, 22, 45, 65, 70],
        NumETA: [123, 234, 345, 456, 567],
        Options: 0
    },
    {
        uniqueID: 'asoi2392345',
        TourSCEMID: 'ABCDEFGHIJ',
        StopSCEMIDS: ['QWERTZU', 'ASDFSDFGJ', 'YXCVBN', 'WREEZUUI', 'RETUKSFGHJ'],
        NumPosEvent: [3, 6, 5, 7, 8],
        NumSubscription: [12, 22, 45, 65, 70],
        NumETA: [123, 234, 345, 456, 567],
        Options: 0
    },
    {
        uniqueID: 'asoi2392345',
        TourSCEMID: 'qwerreqtrtez',
        StopSCEMIDS: ['QWERTZU', 'ASDFSDFGJ', 'YXCVBN', 'WREEZUUI', 'RETUKSFGHJ'],
        NumPosEvent: [3, 6, 5, 7, 8],
        NumSubscription: [12, 22, 45, 65, 70],
        NumETA: [123, 234, 345, 456, 567],
        Options: 0
    }
];