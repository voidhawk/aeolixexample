/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
DAKOSY component implementation

Info:
20171124 | Created

*/

import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatFormField, MAT_DIALOG_DATA } from '@angular/material';

/**
 * This class is used to handle read xml files and convert them into a DTM structure
 */
@Component({
    selector: 'dakosy-dialog',
    templateUrl: 'dakosy.component.html',
})
export class DakosyDlg {

    constructor(public dialogRef: MatDialogRef<DakosyDlg>, @Inject(MAT_DIALOG_DATA) public data: any) {

    }

    onNoClick(): void {

        this.dialogRef.close();
    }
}