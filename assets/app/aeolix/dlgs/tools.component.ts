import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatFormField, MatTooltip, MAT_DIALOG_DATA } from '@angular/material';
import { OneSignalComponent } from '../../shared/dlgs/onesignal/onesignal.component';


@Component({
    selector: 'tools-dialog',
    templateUrl: 'tools.component.html',
})
export class ToolsDlg {

    constructor(public toolsDlgRef: MatDialogRef<ToolsDlg>, public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) {

    }

    onNoClick(): void {

        this.toolsDlgRef.close();
    }
    
    onOneSignal(): void {

        let oneSignalDlg = this.dialog.open(OneSignalComponent, {
            
            width: '400px',
            //height: '200px',
            data: { email: "", password: "" }
        });


        oneSignalDlg.afterClosed().subscribe(result => {

            console.log('The OneSignal dialog was closed');
            //console.log(result);
        });

        this.toolsDlgRef.close();
    }
}