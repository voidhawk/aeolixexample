/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20170711 | Created

*/
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import {
    MatButtonModule, MatTooltipModule, MatCheckboxModule, MatSelectModule,
    MatToolbarModule, MatIconModule, MatSidenavModule, MatDialogModule,
    MatFormFieldModule, MatInputModule, MatExpansionModule, MatListModule, MatDatepickerModule, 
    MatNativeDateModule, MatPaginatorModule, MatTableModule, MatSlideToggleModule
} from '@angular/material';

/*
Translation section
*/
import { HttpModule, Http } from "@angular/http";
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {

    return new TranslateHttpLoader(http, "./i18n/", ".json");
}
import { MongoDBService } from './../datamgmt/mongodb.service';
import { AeolixComponent } from './aeolix.component';

import { ToolsDlg } from './dlgs/tools.component'
import { AeolixDlgSelect, PosEventSelectDlg, NotificationSelectDlg, SubscriptionSelectDlg } from './dlgs/aeolixselect.component';
import { SharedModule } from './../shared/shared.module';
//import { DakosyDlg } from './dlgs/dakosy.component'

@NgModule({
    imports: [
        CdkTableModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatTooltipModule,
        MatSelectModule,
        MatIconModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatExpansionModule,
        MatListModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatPaginatorModule,
        MatTableModule,
        MatSlideToggleModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        SharedModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ],
    declarations: [
        AeolixComponent, ToolsDlg, AeolixDlgSelect, PosEventSelectDlg, NotificationSelectDlg, SubscriptionSelectDlg
    ],
    providers: [
        MongoDBService
        //GoogleMapsAPIWrapper
    ],
    entryComponents: [
        ToolsDlg, AeolixDlgSelect, PosEventSelectDlg, NotificationSelectDlg, SubscriptionSelectDlg
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AeolixModule {

}
