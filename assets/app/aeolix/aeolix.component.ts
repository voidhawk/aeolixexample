/*
Author: Jürgen Stolz / PTV Group
Copyright: 2017
Research project Aeolix

Description: 
Example component

Info:
20170711 | Created

*/
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Http, Response } from '@angular/http';
import { Subscribe, AeonSubscription } from 'thevoidmodelmodule/dist/aeolix';
import { PermittedAction } from 'thevoidmodelmodule/dist/shared';

//import { AeonSubscription } from "thevoidmodelmodule/dist/aeolix/models/subscribe.model";
import { TranslateService } from '@ngx-translate/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import 'rxjs/Rx';
import { Observable } from 'rxjs';
import { Subscription as RXJSSubscription } from 'rxjs/Subscription';

import { MongoDBService } from './../datamgmt/mongodb.service';
var uniquid = require('uniquid');

var fastXmlParser = require('fast-xml-parser');
import { TheVoidDataService } from '../datamgmt/thevoid-data.service';
import { ToolsDlg } from "./dlgs/tools.component";
import { AeolixDlgSelect, PosEventSelectDlg, NotificationSelectDlg, SubscriptionSelectDlg } from "./dlgs/aeolixselect.component";

declare var AeonSDK: any;
//import { AeonSDK } from 'aeonsdk-node';

/**
 * TheVoidModelModule everything using Shared content
 */
import { TheVoidCoordinate } from 'thevoidmodelmodule/dist/shared';
import { TheVoidOriginator } from 'thevoidmodelmodule/dist/shared';
import { TV_User, TV_Role, TV_Permission, TV_Config } from 'thevoidmodelmodule/dist/shared';

/**
 * TheVoidModelModule everything using Logisitics content
 */
import { TOURSTOP_POSITION } from "thevoidmodelmodule/dist/aeolix/models/logistics/logistics.model";

/**
 * TheVoidModelModule everything using Aeolix content
 */
import { Publish, TOUR_HEADER, TOUR_RESOURCE, TOUR_STOP, ExtIdent } from 'thevoidmodelmodule/dist/aeolix';

/**
 * TheVoidModelModule everything using DAve content
 */
import { Tour, TourApiRequestParams } from 'thevoidmodelmodule/dist/dave';
import { Coordinate } from 'thevoidmodelmodule/dist/dave';
import { Vehicle } from 'thevoidmodelmodule/dist/dave';
import { Driver } from 'thevoidmodelmodule/dist/dave';
import { PosEvent, EventPayLoadBase, PosEventApiRequestParams } from 'thevoidmodelmodule/dist/dave';
import { Subscription, SubscriptionCallbackUrlPayLoad, SubscriptionApiRequestParams, NotificationType } from 'thevoidmodelmodule/dist/dave';
import { Stop } from "thevoidmodelmodule/dist/dave/models/stop.model";
import { TV_CoordinateType } from 'thevoidmodelmodule/dist/global-enums';
import { subscribeOn } from "rxjs/operator/subscribeOn";

@Component({
    selector: 'my-app-aeolix',
    templateUrl: "./aeolix.component.html",
    styleUrls: ["./aeolix.component.css"]
})
export class AeolixComponent implements OnInit {

    public jsonContentRequest: any;
    public jsonContentResponse: any;

    /**
     * This parameter is used to enable the button ".Btn_Aeon_Pub_ExtChannel" after
     * the subscription to the channel ExtSubscribe was successfull.
     */
    public subscribed: boolean = false;

    /**
     * This parameter is used to enable the button ".Btn_Aeon_Pub_ExtChannel" after
     * the subscription to the channel ExtSubscribe was successfull.
     */
    public subscribed4Notifications: boolean = false;

    /**
     * The parameter is set to TRUE after the subscription to the NOTIFIACTION channel
     * was successfull and is used to enable the button ".Btn_Resp_SendSubscription".
     */
    public subscribed2Notification: boolean = false;

    public createResponseTourReceived: boolean = false;
    public msgResponseReceived : boolean = true;
    private scemids: string[] = [];
    private token: string;
    private source: string;
    private currentDAveTourURL: string;
    private currentDAvePosEventURL: string;
    private currentDAveLocationURL: string;
    private currentDAveSubscriptionURL: string;

    private fakeUserAndConfiguration: TV_User;

    private locationId: string;

    /**
     * AEON Channel name = ExtPublish
     * ptvPublishURL is the one and only entry into the AEON channel environment => if you want to use
     * PTV services. The default is ExtPublish => 'http://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe'
     */
    private ptvPublishURL: string;

    /**
     * AEON Channel name = ExtSubscribe
     * ptvPublish2ReceiveURL is the default output channel PTV publishes an answer
     * ExtSubscribe => 'http://aeon.atosresearch.eu:3000/publish/2ff0d533-a89d-4864-a2d5-776821fcbb0e';
     */
    private ptvPublish2ReceiveURL: string;

    /**
     * This is the PTV Notification SUBSCRIBE channel. This part is only used from interrested customers
     */
    private subNotificationURL: string;

    private AeolixNodeUrl: string;           //URL of the DB we use
    private content: object[] = [];     //storage for the TourMetaData we get from the Database
    private tourSCEMID: string;
    private showMode: boolean;
    private subscriber: string;
    private subscriptionData: any;
    private subscriptionDataRequest: any;
    private subscriptionDataResponse: any;
    private subscriptionDataNotification: any;
    private mnemonic: string;
    private spyMode: boolean;

    private extPublishSDK: any;

    private file: any;
    private fileContent: any;

    private responseInfoForm: FormGroup;
    position = 'before';
    appLanguage: string;

    /*
    Test
    */
    private userForm: FormGroup;
    private profiles = [
        { name: 'Developer', shortName: 'dev' },
        { name: 'Manager', shortName: 'man' },
        { name: 'Director', shortName: 'dir' }
    ];

    private rxjsSubscription: RXJSSubscription;

    constructor(private theVoidDataService: TheVoidDataService, private dialog: MatDialog, public translate: TranslateService,
        private http: Http, private mongoDB: MongoDBService) {

        /** 
         * Translation part 
         * To set a language use => translate.use('de');
         */
        this.translate.addLangs(["en", "fr", "de", "gr", "it", "es"]);
        this.translate.setDefaultLang('en');

        let browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|de|gr|it|es/) ? browserLang : 'en');

        /**
         * Internal MESSAGING part
         */
        this.theVoidDataService.userChangedStream$.subscribe(item => { this.onUserChanged(item); });
    }

    onUserChanged(item: TV_User) {

        if (item != null) {

            console.log('onUserChanged: ' + JSON.stringify(item));
            this.source = this.theVoidDataService.getEMailAddress();
            let publishTVConfig: TV_Config;
            publishTVConfig = this.theVoidDataService.getRolePermissionOfUser("Aeolix", "PUBLISH", "ExtPublish");
            if (publishTVConfig != null) {

                this.ptvPublishURL = publishTVConfig.value;
                //this.ptvPublishURL = 'https://aeon.atosresearch.eu:3000/publish/0e71ed66-44f0-4045-bb05-458e8155f2fe';
                console.log(JSON.stringify(publishTVConfig, null, 4));
            }

            publishTVConfig = this.theVoidDataService.getRolePermissionOfUser("Aeolix", "PUBLISH", "ExtSubscribe");
            if (publishTVConfig != null) {

                this.ptvPublish2ReceiveURL = publishTVConfig.value;
                console.log(JSON.stringify(publishTVConfig, null, 4));
            }

            let tvPermission: TV_Permission;
            tvPermission = this.theVoidDataService.getRolePermissionOfUser("Aeolix", "SUBSCRIBE");
            console.log("Role=Aeolix, Permission=SUBSCRIBE: " + JSON.stringify(tvPermission, null, 4));
            for (var subscribeConfig of tvPermission.configs) {

                console.log('Subscribe to channel(' + subscribeConfig.key + ')');
                this.subscribe2Channel(subscribeConfig);
            }

            localStorage.setItem('AeolixSource', this.source);
        }
        else {

            /**
             * ToDo: notwendige Aktionen ergänzen
             */

            // Pause or unsubscribe
        }
    }

    /**
     * Diese Methode soll aufgerufen werden, wenn ein TV_User geladen wurde.
     * 
     * @param subscription
     */
    subscribe2Channel(subscriptionConfig: TV_Config) {

        console.log("subscribe2Channel called: " + JSON.stringify(subscriptionConfig));
        if (subscriptionConfig.action === undefined) {

            subscriptionConfig.action = PermittedAction.USE;
        }
        if (subscriptionConfig.action != null) {

            if (subscriptionConfig.action === PermittedAction.NONE) {

                return;
            }
        }

        var sdk = new AeonSDK(subscriptionConfig.value.subscriptionUrl, subscriptionConfig.value.subscription);
        var identifier = subscriptionConfig.value.subscription.desc;

        /**
         * 
         * @param msg This is the callback where I receive the Aeon control messages
         */
        var control = function control(controlMsg) {

            console.log("AeolixComponent.onSubscribe(" + identifier + ") => Callback function control(): ", controlMsg);
            if (controlMsg.code == 250) { //you have been subscribed

                var persistantSubscription = sdk.getSubscription();
                console.log("Identifier=" + identifier + ": " + persistantSubscription);
                //logger.notice({ info: 'AEON Callback control():', msg: persistantSubscription });
                // store the persistantSubscription to be used in the future
            }
        }

        var that = this;
        /**
         * 
         * @param msg This is the callback where I expect my notification of the AEON framework
         */
        var received = function received(msg) {

            console.log("Subscriber: " + sdk.getSubscription().desc + ", Content: " + JSON.stringify(msg));
            if (msg.notificationContentType !== undefined) {

                var contentAsJson = JSON.stringify(msg, null, 4);
                that.jsonContentRequest = contentAsJson;

                return;
            }
            else if (msg.originator.source == that.theVoidDataService.getEMailAddress() || that.spyMode == true) {

                console.log(sdk.getSubscription());
                if (msg.service === "DAve") {
                    var contentAsJson = JSON.stringify(msg, null, 4);   
                    //  that.jsonContentResponse = contentAsJson;
                    if (msg.subType === "Tour" && msg.data.responseStatus && !that.createResponseTourReceived) {
                        console.log("AeolixComponent.received() => Callback function received(Tour): ", msg);
                        that.scemids = [];
                        that.createResponseTourReceived = true;

                        for (var index = 0; index < msg.data.tour.stops.length; index++) {
                            var element = msg.data.tour.stops[index];
                            that.scemids.push(element.scemid);
                        }
                    }
                    else if (msg.subType === "PosEvent") {
                        console.log("AeolixComponent.received() => Callback function received(PosEvent): ", msg);
                    }
                }
                else if (msg.service === "xRoute") {

                    console.log("AeolixComponent.received() => Callback function received(xRoute): ", msg);
                }

                if (msg.data.responseStatus && !that.msgResponseReceived) {
                    console.log('Response was received');
                    that.jsonContentResponse = contentAsJson;
                } else if (!msg.data.responseStatus && that.msgResponseReceived) {
                    console.log('Request was received');
                    that.jsonContentRequest = contentAsJson;
                }

                if(msg.data.responseStatus && !that.msgResponseReceived || !msg.data.responseStatus && that.msgResponseReceived){
                that.mongoDB.postContent(contentAsJson, that.AeolixNodeUrl).subscribe(
                    data => console.log(data),
                    error => console.error(error)
                );
                that.msgResponseReceived = !that.msgResponseReceived;
            }
            }
            else {

                console.error("NOT processed: Subscriber: " + sdk.getSubscription().desc + ", Content: " + JSON.stringify(msg));
            }
        }




        /**
        * Her I subscribe to the Aeon channel with the 2 callbacks
        */
        sdk.subscribe(received, control);

        //sdk.continueSubscription();
        console.log("AeolixComponent: Ok, we are subscribed, waiting for messages");
        this.subscribed = true;


    }

    onPublishTour_Icon($event) {
        this.createResponseTourReceived=false;
        this.onPublishTour(event);
    }

    onPublishTour_Button($event) {

        this.onPublishTour(event);
    }

    onPublishTour($event) {

        /*
        I use a so call Originator because I want to know who is accessing me.
        */
        var originator = new TheVoidOriginator(this.mnemonic);
        originator.source = this.source;

        /*
        The Publish part contains the content I expect as data in my subscription
        */
        var publishMsg = new Publish();
        publishMsg.originator = originator;
        publishMsg.creationTime = new Date();

        /**
         * This parameter describes the Service I want to acces in my middleware
         */
        publishMsg.service = "DAve";
        /**
         * and in the DAve Service I want to create a Tour
         */
        publishMsg.subType = 'Tour';

        publishMsg.publish2Url = this.ptvPublishURL;
        publishMsg.publishResp2Url = this.ptvPublish2ReceiveURL;

        /**
         * This action creates a sample tour with 2 stops
         */
        let daveTour: Tour;
        daveTour = this.createPTVTour();
        var useConverter = false;
        //JSt: Rechte und Rollen
        if (useConverter) {

            var tourHeader = this.convertPTVTour_2_DTM(daveTour, this.token, this.currentDAveTourURL, this.source);
            publishMsg.data = tourHeader;
        }
        else {

            /**
             * The TourApiRequestParams object contains the SecurityToken (DAve), The API I want to access from my
             * middleware and the content created above
             */
            var tourApiRequestParams = new TourApiRequestParams(this.token, this.currentDAveTourURL, this.source);
            tourApiRequestParams.tour = daveTour;
            publishMsg.data = tourApiRequestParams;
        }

        /**
         * This is the AEON publish part, their SDK provides a publish function. I call this function
         * with the content above and a callback to receive a Aeon response.
         */
        // We send the content to an AEON channel
        this.publishContent2Channel(publishMsg);
    }

    createTestTour(){

        var tour = {
                "source": "NoscifelPlatform",
                "url": "https://eta.cloud.ptvgroup.com/em/tour",
                "tour": {
                    "customData": {
                        "idDestination": -1,
                        "idOrdreMission": -1,
                        "idChauffeur": "160"
                    },
                    "source": "NoscifelPlatform",
                    "stops": [{
                        "stopPositionInTour": 1,
                        "earliestArrivalTime": "2017-12-15T18:00:00.000+01:00",
                        "coordinate": {
                            "locationY": 43.42034912109375,
                            "locationX": 4.826311111450195,
                            "isValid": false
                        },
                        "weightWhenLeavingStop": -1,
                        "latestDepartureTime": "2017-12-15T20:00:00.000+01:00",
                        "customData": {
                            "idDestination": 2213,
                            "idOrdreMission": 501,
                            "type": "CHARGEMENT"
                        },
                        "useServicePeriodForRecreation": false,
                        "serviceTimeAtStop": 2700
                    }, {
                        "stopPositionInTour": 2,
                        "earliestArrivalTime": "2017-12-15T21:00:00.000+01:00",
                        "coordinate": {
                            "locationY": 43.69615936279297,
                            "locationX": 6.9765777587890625,
                            "isValid": false
                        },
                        "weightWhenLeavingStop": -1,
                        "latestDepartureTime": "2017-12-15T23:00:00.000+01:00",
                        "customData": {
                            "idDestination": 2509,
                            "idOrdreMission": 501,
                            "type": "LIVRAISON"
                        },
                        "useServicePeriodForRecreation": false,
                        "serviceTimeAtStop": 2700
                    }],
                    "drivers": [{
                        "driverDescription": "FaureCha1"
                    }],
                    "vehicle": {
                        "vehicleProfileID": "mg-truck-40t"
                    }
                },
                "token": "FSA2JNTAXDNY4NGR9JS54ZUIP"
        }
        return tour;
    }

    /**
     * This method returns a tour which contains vehicle data (mg-truck-40t => do not change), 
     * a driver and 2 stops (including a locationId at the second stop. 
     * 
     * Tour, Stop, Vehicle, Driver are models you find in DAve.
     */
    createPTVTour() {

        var daveTour = new Tour();
        daveTour.vehicle = new Vehicle("mg-truck-40t");
        daveTour.drivers.push(new Driver("driver_1"));

        var firstStopCoordinate = new Coordinate();
        firstStopCoordinate.locationX = 12.134485244751;
        firstStopCoordinate.locationY = 45.5063469010834;

        /**
         * the createAndAddStop method fill the following parameters
         * => this first is the coordinate 
         * => the second contains the ServiceTimeAtStop, which mean how long did the driver
         * stays at this stop (loadin, unloading)
         * => the third contains the position of a stop in the tour. If this parameter
         * is not unique in the trip, the stop sequence is the samme as the array sequence you provide
         * (0, 2, 8, 2, 1, 3) 2 == duplicated so we use the given sequence
         */
        daveTour.createAndAddStop(firstStopCoordinate, 600, 0);
        var secondStopCoordinate = new Coordinate();
        secondStopCoordinate.locationX = 13.764221;
        secondStopCoordinate.locationY = 45.649556;
        daveTour.createAndAddStop(secondStopCoordinate, 1200, 1);

        /**
         * The locationId is the Id of a location with an ArrivalBoard.
         */
        daveTour.stops[1].locationId = "DYPY823I8BCGQJ5D7GQZHZSQG";

        return daveTour;
    }

    onSendPosEvent() {

        var selectedScemid = this.responseInfoForm.get('scemids').value;
        var currentStatus = this.responseInfoForm.get('scemids').status;
        console.log("Selected scemid=" + selectedScemid);

        var originator = new TheVoidOriginator(this.mnemonic);
        originator.source = this.source;
        var publishMsg = new Publish();
        publishMsg.originator = originator;
        publishMsg.creationTime = new Date();
        publishMsg.service = "DAve";
        publishMsg.subType = 'PosEvent';

        publishMsg.publish2Url = this.ptvPublishURL;
        publishMsg.publishResp2Url = this.ptvPublish2ReceiveURL;

        /** erzeugen eines PosEvents für DAve */
        var posEventScemid = this.responseInfoForm.get('scemids').value;
        var davePosEvent = this.createPTVPosEvent(posEventScemid);

        var useConverter = false;
        //JSt: Rechte und Rollen
        if (useConverter) {

            var tourHeader = this.convertPTVPosition_2_DTM(davePosEvent, this.currentDAvePosEventURL, this.source);
            publishMsg.data = tourHeader;
        }
        else {

            var posEventApiRequestParams = new PosEventApiRequestParams(this.currentDAvePosEventURL, this.source);
            posEventApiRequestParams.posEvent = davePosEvent;
            publishMsg.data = posEventApiRequestParams;
        }

        // We send the content to an AEON channel
        this.publishContent2Channel(publishMsg);
    }

    createPTVPosEvent(posEventScemid) {

        var davePosEvent = new PosEvent();
        davePosEvent.scemid = posEventScemid;
        var daveCoordinate = new Coordinate();
        daveCoordinate.locationX = 12.47772216796875;
        daveCoordinate.locationY = 45.66012730272194;
        var eventPayLoadBase = new EventPayLoadBase();
        eventPayLoadBase.coordinate = daveCoordinate;
        eventPayLoadBase.accuracy = 10;
        eventPayLoadBase.speed = 25;
        eventPayLoadBase.heading = 270;
        var posDate: Date;
        eventPayLoadBase.setTimeStamp(new Date());
        davePosEvent.eventPayLoad = eventPayLoadBase;
        davePosEvent.visibility = true;

        return davePosEvent;
    }

    /**
     * Erzeugen einer Subscription für DAve. In diesem Umfeld werden nur
     * CALLBACK Notifications verwendet
     */
    onSendSubscription() {

        var selectedScemid = this.responseInfoForm.get('scemids').value;
        var currentStatus = this.responseInfoForm.get('scemids').status;
        console.log("onSendSubscription(Selected scemid=" + selectedScemid + ")");

        var originator = new TheVoidOriginator(this.mnemonic);
        originator.source = this.source;
        var publishMsg = new Publish();
        publishMsg.originator = originator;
        publishMsg.creationTime = new Date();
        publishMsg.service = "DAve";
        publishMsg.subType = 'Subscription';

        publishMsg.publish2Url = this.ptvPublishURL;
        publishMsg.publishResp2Url = this.ptvPublish2ReceiveURL;

        /** erzeugen einer Subscription für DAve */
        var daveSubscription = new Subscription();
        daveSubscription.notificationType = NotificationType.CALLBACK_URL;
        daveSubscription.scemid = selectedScemid;
        var daveSubscriptionCallback = new SubscriptionCallbackUrlPayLoad();

        /**
         * Hier müssen wir die PUBLISH url des Notification Channels übergeben
         * Channel "Notification" (production zone)
         *  PUBLISH=> "https://aeon.atosresearch.eu:3000/publish/0d77bcbe-e7fc-4c69-acc7-b55b08382f1f";
         *  SUBSCRIBE=> "https://aeon.atosresearch.eu:3000/subscribe/73c9f42e-fb75-4f16-bb85-9bd1aca4a90c";
         */
        var notificationPublishChannel = this.theVoidDataService.getRolePermissionOfUser("Aeolix", "PUBLISH", "Notification");
        daveSubscriptionCallback.callBackURL = notificationPublishChannel.value;
        daveSubscription.notificationDescription = daveSubscriptionCallback;

        var subscriptionApiRequestParams = new SubscriptionApiRequestParams(this.currentDAveSubscriptionURL, this.source);
        subscriptionApiRequestParams.subscription = daveSubscription;

        publishMsg.data = subscriptionApiRequestParams;

        var contentAsJson = JSON.stringify(publishMsg, null, 4);

    
        // We send the content to an AEON channel
        this.publishContent2Channel(publishMsg);
    }

    publishContent2Channel(publishMsg) {

        var that = this;
        that.extPublishSDK = new AeonSDK(this.ptvPublishURL);
        that.extPublishSDK.publish(publishMsg, function (result) {

            if (result.code === 200) {

                console.log("SUCCESS: publishContent2Channel(" + that.ptvPublishURL + ")");
            } else {
                console.error('An Error occured.');
            }
        });
    }

    onChange($event) {

    }

    onChangeLangSel($event) {

        this.translate.use($event.value);
    }

    onGetDAveLocations($event) {

        console.log("AeolixComponent.onGetDAveLocations() called");
        /*
        var locations = this.daveService.getLocationsOfToken(this.aeolixDataService.currentDAveToken)
            .subscribe(
            (locations: TheVoidLocation[]) => {
 
                locations.forEach(location => {
                    this.aeolixDataService.addLocation(location);
                });
 
            }
            );
        */
        console.log("AeolixComponent.onGetDAveLocations() finished");
    }

    onFormSubmit(): void {
        console.log('Name:' + this.userForm.get('name').value);
        /*
        this.userForm.setValue({name: 'Mahesh', age: '20' }); 
        this.userForm.patchValue({name: 'Mahesh'});
 
        this.userForm.get('profile').value;
        */
    }

    onRefresh() {

        var uniqueIDToGet: string;

        let dialogRef = this.dialog.open(AeolixDlgSelect, {
            width: '1000px',
            height: '800px',
            data: {
                uniqueID: uniqueIDToGet
            }
        });

        dialogRef.afterClosed().subscribe(result => {

            console.log('The dialog was closed');
            this.mongoDB.getContentToShow(this.AeolixNodeUrl, 'Tour', result)
                .subscribe(
                content => {
                    this.jsonContentRequest = JSON.stringify(content[0], null, 4);
                    this.jsonContentResponse = JSON.stringify(content[1], null, 4);
                    this.tourSCEMID = content[1].data.tour.scemid;
                    console.log(this.tourSCEMID);
                    this.showMode = true;
                },
                error => console.error(error)
                );


        });
    }

    /**
HEAD

               for (var i = 0; i < content[0].data.tour.stops.length; i++) {
                    scemids.push(content[0].data.tour.stops[i].scemid);
=======
                debugger;
               for (var i = 0; i < content.length; i++) {
                    scemids.push(content[i].data.posEvent.scemid);
74a088a3457387e7ccdb9cbb35dedfd862a6f193
     */
    OpenPosEventDlg() {

        var scemids: string[] = [];
        var uniqueIDToGet: string = JSON.parse(this.jsonContentRequest).uniqueID;
        var scemidToGet: string;

        this.mongoDB.getOverviewByID(this.AeolixNodeUrl, 'PosEvent', uniqueIDToGet)
            .subscribe(
            content => {
                debugger;
               for (var i = 0; i < content.length; i++) {
                    scemids.push(content[i].data.posEvent.scemid);
                }
                let dialogRef = this.dialog.open(PosEventSelectDlg, {
                    width: '800px',
                    height: '600px',
                    data: {
                        scemids: scemids,
                        scemid: scemidToGet
                    }
                });

                dialogRef.afterClosed().subscribe(result => {
                    console.log('The dialog was closed');

                    this.mongoDB.getContentToShow(this.AeolixNodeUrl, 'PosEvent', JSON.parse(this.jsonContentRequest).uniqueID, result)
                        .subscribe(
                        content => {
                            this.jsonContentResponse = JSON.stringify(content, null, 4);
                            console.log(this.tourSCEMID);
                            this.showMode = true;
                        },
                        error => console.error(error)
                        );

                });
            },
            error => console.error(error)
            );
        console.log(scemids);

    }

    OpenNotificationDlg(uniqueIDToGet: string, scemidToGet: string) {

        let dialogRef = this.dialog.open(NotificationSelectDlg, {
            width: '800px',
            height: '600px',
            data: {
                uniqueID: uniqueIDToGet,
                scemid: scemidToGet
            }
        });

        dialogRef.afterClosed().subscribe(result => {

            console.log('The dialog was closed');
            this.mongoDB.getContentToShow(this.AeolixNodeUrl, 'Notification', result.uniqueID, result.scemid)
                .subscribe(
                content => {
                    this.jsonContentRequest = JSON.stringify(content, null, 4);
                    console.log(this.tourSCEMID);
                    this.showMode = true;
                },
                error => console.error(error)
                );


        });
    }

    /**
HEAD

                for (var i = 0; i < content[0].data.tour.stops.length; i++) {
                    scemids.push(content[0].data.tour.stops[i].scemid);
=======
                debugger;
                for (var i = 0; i < content.length; i++) {
                    scemids.push(content[i].data.posEvent.scemid);
74a088a3457387e7ccdb9cbb35dedfd862a6f193
     */
    OpenSubscriptionDlg() {

        var scemids: string[] = [];
        var uniqueIDToGet: string = JSON.parse(this.jsonContentRequest).uniqueID;
        var scemidToGet: string;

        this.mongoDB.getOverviewByID(this.AeolixNodeUrl, 'Subscription', uniqueIDToGet)
            .subscribe(
            content => {
                debugger;
                for (var i = 0; i < content.length; i++) {
                    scemids.push(content[i].data.posEvent.scemid);
                }
                let dialogRef = this.dialog.open(SubscriptionSelectDlg, {
                    width: '800px',
                    height: '600px',
                    data: {
                        scemids: scemids,
                        scemid: scemidToGet
                    }
                });

                dialogRef.afterClosed().subscribe(result => {
                    console.log(result);
                    console.log('The dialog was closed');
                    this.mongoDB.getContentToShow(this.AeolixNodeUrl, 'Subscription', JSON.parse(this.jsonContentRequest).uniqueID, result)
                        .subscribe(
                        content => {
                            this.jsonContentResponse = JSON.stringify(content, null, 4);
                            this.showMode = true;
                        },
                        error => console.error(error)
                        );

                });
            },
            error => console.error(error)
            );

    }

    resetForm() {

        this.userForm.reset();
    }

    ngOnInit() {
        this.spyMode = false;
        this.token = "FSA2JNTAXDNY4NGR9JS54ZUIP"
        this.source = 'theo_test' //<= JSt: please use your own source
        this.locationId = "DYPY823I8BCGQJ5D7GQZHZSQG"; // Port Trieste
        this.subscriber = "thej_aeon_dev@aeolix.eu"; // <= JSt: please use your own email
        this.subscriptionDataResponse = { "id": this.subscriber, "desc": 'bhk8mjmx8c80' };
        this.subscriptionDataRequest = { "id": this.subscriber, "desc": 'bhk8mjmxpko0a' };

        this.mnemonic = "PTV AG (Aeolix)";
        this.currentDAveTourURL = "https://eta.cloud.ptvgroup.com/em/tour";
        this.currentDAvePosEventURL = "https://eta.cloud.ptvgroup.com/em/event";
        this.currentDAveLocationURL = "https://driveandarrive-v1.cloud.ptvgroup.com/em/location";
        this.currentDAveSubscriptionURL = "https://driveandarrive-v1.cloud.ptvgroup.com/em/subscription";

        /*
        Test
        */
        this.responseInfoForm = new FormGroup({

            scemids: new FormControl(this.scemids)
        });

        this.userForm = new FormGroup({

            name: new FormControl(),
            age: new FormControl('20'),
            gender: new FormControl('male'),
            profile: new FormControl(this.profiles[0].shortName)
        });
        this.AeolixNodeUrl = 'http://localhost:3000/aeolix_server';
        this.showMode = false;
    }

    syntaxHighlight(json: any) {

        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    convertDTM_2_PTVTour(dtm: TOUR_HEADER) {

        let tour: Tour;
        tour.stops = [];
        for (var index = 0; index < dtm.TOUR_STOPs.length; index++) {

            var stop = new Stop();
            stop.serviceTimeAtStop = dtm.TOUR_STOPs[index].SERVICE_PERIOD;
            stop.stopPositionInTour = dtm.TOUR_STOPs[index].TOURPOINT_SEQUENCE;
            stop.useServicePeriodForRecreation = dtm.TOUR_STOPs[index].REST_PERIOD_AT_STOP ? true : false;
            stop.weightWhenLeavingStop = dtm.TOUR_STOPs[index].CURRENT_WEIGHT;
            stop.coordinate = new Coordinate();
            stop.coordinate.locationX = dtm.TOUR_STOPs[index].LONGITUDE;
            stop.coordinate.locationY = dtm.TOUR_STOPs[index].LATITUDE;
            stop.earliestArrivalTime = dtm.TOUR_STOPs[index].ARRIVAL_TIME;
            stop.latestDepartureTime = dtm.TOUR_STOPs[index].DEPARTURE_TIME;

            if (dtm.TOUR_STOPs[index].EXTIDs != null && dtm.TOUR_STOPs[index].EXTIDs.length > 0) {

                for (var extIdIndex = 0; extIdIndex < dtm.TOUR_STOPs[index].EXTIDs.length; extIdIndex++) {

                    let extIdContent = dtm.TOUR_STOPs[index].EXTIDs[extIdIndex];
                    if (extIdContent.type === "LocationId") {

                        stop.locationId = extIdContent.extId;
                    }
                }
            }
            tour.stops.push(stop);
        }

        tour.drivers = [];
        if (dtm.TOUR_RESOURCEs != null && dtm.TOUR_RESOURCEs.length > 0) {

            for (var tourResourceIndex = 0; tourResourceIndex < dtm.TOUR_RESOURCEs.length; tourResourceIndex++) {

                let tourResource = dtm.TOUR_RESOURCEs[tourResourceIndex];
                if (tourResource.TYPE === "Driver") {

                    let driver: Driver;
                    driver.driverDescription = tourResource.NAME;

                    tour.drivers.push(driver);
                }
                else if (tourResource.TYPE === "Vehicle") {

                    tour.vehicle = new Vehicle(tourResource.EXTIDs[0].extId);
                }
            }
        }

        return tour;
    }

    convertPTVTour_2_DTM(tour: Tour, token: string, url: string, source: string) {

        var tourHeader = new TOUR_HEADER();
        tourHeader.EXTIDs = [];
        /**
        * We fill the TOUR_HEADER
        */
        var tokenExtId = new ExtIdent();
        tokenExtId.type = "TourToken";
        tokenExtId.extId = token;
        tourHeader.EXTIDs.push(tokenExtId);

        var urlExtId = new ExtIdent();
        urlExtId.type = "TourApiURL";
        urlExtId.extId = url;
        tourHeader.EXTIDs.push(urlExtId);

        var sourceExtId = new ExtIdent();
        sourceExtId.type = "Source";
        sourceExtId.extId = source;
        tourHeader.EXTIDs.push(sourceExtId);

        tourHeader.START_DATETIME = "2017-11-24 12:00:00";//new Date();

        /**
        * This parameter describes the Service I want to acces in my middleware
        */
        //publishMsg.service = "DAve";
        var serviceExtId = new ExtIdent();
        serviceExtId.type = "Service";
        serviceExtId.extId = "DAve";
        tourHeader.EXTIDs.push(serviceExtId);
        /**
         * and in the DAve Service I want to create a Tour
         */
        //publishMsg.subType = 'Tour';
        var subTypeExtId = new ExtIdent();
        subTypeExtId.type = "SubType";
        subTypeExtId.extId = "Tour";
        tourHeader.EXTIDs.push(subTypeExtId);

        //publishMsg.publish2Url = this.ptvPublishURL;
        //publishMsg.publishResp2Url = this.ptvPublish2ReceiveURL;

        /**
         * We fill the TOUR_RESOURCEs into the TOUR_HEADER
         */
        tourHeader.TOUR_RESOURCEs = [];

        // First the drivers
        let drivers: Driver[];
        drivers = tour.drivers;
        for (var index = 0; index < drivers.length; index++) {

            var driver = new TOUR_RESOURCE();
            driver.TYPE = "Driver";
            driver.EXTIDs = [];
            driver.NAME = drivers[index].driverDescription;
            /*
            var transportMeansID = new ExtIdent();
            transportMeansID.type = "Mobile";
            transportMeansID.extId = "0049/721/9651-476-" + index;
            driver.EXTIDs.push(transportMeansID);
            */

            tourHeader.TOUR_RESOURCEs.push(driver);
        }

        // Second the assigned transportation resources
        let vehicle: Vehicle;
        vehicle = tour.vehicle;
        var vehicleResource = new TOUR_RESOURCE();
        vehicleResource.TYPE = "Vehicle";
        vehicleResource.EXTIDs = [];
        var vehicleProfileID = new ExtIdent();
        vehicleProfileID.type = "VehicleProfileID";
        vehicleProfileID.extId = vehicle.vehicleProfileID;
        vehicleResource.EXTIDs.push(vehicleProfileID);
        // vehicleResource.NAME = NOT USED
        tourHeader.TOUR_RESOURCEs.push(vehicleResource);

        /**
         * We fill the TOUR_STOPs into the TOUR_HEADER
         */
        tourHeader.TOUR_STOPs = [];
        for (var index = 0; index < tour.stops.length; index++) {

            var stop = new TOUR_STOP();
            stop.EXTIDs = [];
            stop.SERVICE_PERIOD = tour.stops[index].serviceTimeAtStop;
            stop.TOURPOINT_SEQUENCE = tour.stops[index].stopPositionInTour;
            stop.REST_PERIOD_AT_STOP = tour.stops[index].useServicePeriodForRecreation ? 1 : 0;
            stop.CURRENT_WEIGHT = tour.stops[index].weightWhenLeavingStop;
            stop.LONGITUDE = tour.stops[index].coordinate.locationX;
            stop.LATITUDE = tour.stops[index].coordinate.locationY;
            stop.ARRIVAL_TIME = tour.stops[index].earliestArrivalTime;
            stop.DEPARTURE_TIME = tour.stops[index].latestDepartureTime;

            if (tour.stops[index].locationId != null) {

                var locationId = new ExtIdent();
                locationId.type = "LocationId";
                locationId.extId = tour.stops[index].locationId;
                stop.EXTIDs.push(locationId);
            }
            tourHeader.TOUR_STOPs.push(stop);
        }

        return tourHeader;
    }

    convertPTVPosition_2_DTM(posEvent: PosEvent, url: string, source: string) {

        var tourHeader = new TOUR_HEADER();
        tourHeader.EXTIDs = [];
        /**
        * We fill the TOUR_HEADER
        */
        var urlExtId = new ExtIdent();
        urlExtId.type = "PosApiURL";
        urlExtId.extId = url;
        tourHeader.EXTIDs.push(urlExtId);

        var sourceExtId = new ExtIdent();
        sourceExtId.type = "Source";
        sourceExtId.extId = source;
        tourHeader.EXTIDs.push(sourceExtId);

        /**
         * We fill the stop(s), with position(s)
         */
        tourHeader.TOUR_STOPs = [];
        var stop = new TOUR_STOP();
        stop.EXTIDs = [];

        var positionEvent = new TOURSTOP_POSITION
    }

    convertPTVSubscription_2_DTM(posEvent: PosEvent, token: string, url: string, source: string) {

    }

    /**
    * 
    * @param event This method is currently used to create TOUR_HEADER (DTM) using an
    * existing XML-file.
    */
    convertDakosy_2_DTM(event) {

        var xmlData = this.fileContent;
        var jsonObj = fastXmlParser.parse(xmlData);
        console.log(JSON.stringify(jsonObj))
        return;
/*
        // when a tag has attributes
        var options = {
            attrPrefix: "@_",
            textNodeName: "#text",
            ignoreNonTextNodeAttr: true,
            ignoreTextNodeAttr: true,
            ignoreNameSpace: true,
            ignoreRootElement: false,
            textNodeConversion: true,
            textAttrConversion: false,
            arrayMode: false
        };
        if (fastXmlParser.validate(xmlData) === true) {//optional

            var jsonObj = fastXmlParser.parse(xmlData, options);
            console.log(jsonObj);
            var tourHeader = new TOUR_HEADER();
            tourHeader.EXTIDs = [];
            tourHeader.START_DATETIME = jsonObj.Status.Interchange.CreationTime;

            var exchangeNumber = new ExtIdent();
            exchangeNumber.type = "ExchangeNumber";
            exchangeNumber.extId = jsonObj.Status.Interchange.ExchangeNumber;
            tourHeader.EXTIDs.push(exchangeNumber);

            var typeCode = new ExtIdent();
            typeCode.type = "TypeCode";
            typeCode.extId = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.AdditionalReferences.TypeCode;
            tourHeader.EXTIDs.push(typeCode);

            var impReferenceID = new ExtIdent();
            impReferenceID.type = "IMPReferenceID";
            impReferenceID.extId = jsonObj.Status.StatusMessage[0].StatusMessageDetailType.ImportReference.IMPReferenceID;
            tourHeader.EXTIDs.push(impReferenceID);

            var containerID = new ExtIdent();
            containerID.type = "ContainerID";
            containerID.extId = jsonObj.Status.StatusMessage[0].StatusMessageDetailType.ContainerDetails.ContainerID;
            tourHeader.EXTIDs.push(containerID);

            tourHeader.TOUR_STOPs = [];
            var messageCount = jsonObj.Status.StatusMessage.length;
            for (var index = 0; index < messageCount; index++) {

                var tourStop = new TOUR_STOP();
                tourStop.EXTIDs = [];

                var messageReferenceNumber = new ExtIdent();
                messageReferenceNumber.type = "MessageReferenceNumber";
                messageReferenceNumber.extId = jsonObj.Status.StatusMessage[index].MessageHeader.MessageReferenceNumber;
                tourStop.EXTIDs.push(messageReferenceNumber);

                var previousMessageReferenceNumber = new ExtIdent();
                previousMessageReferenceNumber.type = "PreviousMessageReferenceNumber";
                previousMessageReferenceNumber.extId = jsonObj.Status.StatusMessage[index].MessageHeader.PreviousMessageReferenceNumber;
                tourStop.EXTIDs.push(previousMessageReferenceNumber);

                var locationId = new ExtIdent();
                locationId.type = "LocationID";
                locationId.extId = jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.EventLocation.LocationID;
                tourStop.EXTIDs.push(locationId);

                var subLocationOneID = new ExtIdent();
                subLocationOneID.type = "SubLocationOneID";
                subLocationOneID.extId = jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.EventLocation.SubLocationOne.LocationID;
                tourStop.EXTIDs.push(subLocationOneID);

                tourStop.TOURPOINT_SEQUENCE = index + 1;
                tourStop.EXEC_TOURPOINT_STATE = jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.Status.StatusCode;
                tourStop.TEXT = [];
                tourStop.TEXT.push(jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.Status.StatusDescription);
                var dateTime = jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.EventDateTime.EventDate + " " + jsonObj.Status.StatusMessage[index].StatusMessageHeaderType.EventDateTime.EventTime;
                tourStop.EXEC_TOURPOINT_STATE_TIME = dateTime;

                tourHeader.TOUR_STOPs.push(tourStop);
            }

            tourHeader.TOUR_RESOURCEs = [];

            var vessel = new TOUR_RESOURCE();
            vessel.EXTIDs = [];
            var transportMeansID = new ExtIdent();
            transportMeansID.type = "TransportMeansID";
            transportMeansID.extId = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.TransportMeans.TransportMeansID;
            vessel.EXTIDs.push(transportMeansID);

            var callSign = new ExtIdent();
            callSign.type = "CallSign";
            callSign.extId = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.TransportMeans.CallSign;
            vessel.EXTIDs.push(callSign);

            vessel.NAME = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.TransportMeans.TransportMeansName

            tourHeader.TOUR_RESOURCEs.push(vessel);

            var location = new TOUR_RESOURCE();
            location.EXTIDs = [];
            var locationID = new ExtIdent();
            locationID.type = "LocationID";
            locationID.extId = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.EventLocation.LocationID;
            location.EXTIDs.push(locationID);

            var subLocationOne = new ExtIdent();
            subLocationOne.type = "SubLocationOne";
            subLocationOne.extId = jsonObj.Status.StatusMessage[0].StatusMessageHeaderType.EventLocation.SubLocationOne.LocationID;
            location.EXTIDs.push(subLocationOne);

            tourHeader.TOUR_RESOURCEs.push(location);

            var publishMsg = new Publish();
            publishMsg.creationTime = new Date();
            var test = { "TOUR_HEADER": tourHeader };
            publishMsg.data = test;
            var contentAsJson = JSON.stringify(publishMsg, null, 4);
            this.jsonContentRequest = contentAsJson;
        }
*/
        /**
         * Intermediate obj: liefert das gleiche Ergebnis
         */
        //var tObj = fastXmlParser.getTraversalObj(xmlData,options);
        //var jsonObj = fastXmlParser.convertToJson(tObj);
    }

    loadFile(event) {

        const eventObj: MSInputMethodContext = <MSInputMethodContext>event;
        const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
        const files: FileList = target.files;
        this.file = files[0];

        const reader = new FileReader();
        reader.readAsText(this.file, 'utf8');

        var that = this;
        reader.onloadend = function () {

            that.fileContent = reader.result;
        }
    }

    onOpenTools(event) {

        let dialogRef = this.dialog.open(ToolsDlg, {
            width: '400px',
            height: '200px',
            data: { email: "this.user", password: "this.password" }
        });

        dialogRef.afterClosed().subscribe(result => {

            console.log('The dialog was closed');
            //this.password = result;
            //console.log(result);
        });
    }
}