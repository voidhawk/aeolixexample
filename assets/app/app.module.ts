import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
//import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { BrowserModule } from '@angular/platform-browser';

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconRegistry, MatIconModule } from '@angular/material';
/*
Translation section
*/
/*
import { HttpModule, Http } from "@angular/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, "i18n/", ".json");
}
*/
/*
Enable routing, configured in an own file. Don´t forget to import in the ngmodules.imports[].
*/
import { routing } from "./app.routing";

import { AppComponent } from "./app.component";

import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

import { AeolixModule } from './aeolix/aeolix.module';

import { AuthComponent } from './auth/auth.component';
import { AuthModule } from './auth/auth.module'

import { OneSignalComponent } from './shared/dlgs/onesignal/onesignal.component'
import { SharedModule } from './shared/shared.module'

import { DAveService } from './dave/dave.service';
import { TheVoidDataService } from './datamgmt/thevoid-data.service';

@NgModule({
    declarations: [AppComponent, FileSelectDirective],
    imports: [
        BrowserModule,
        FlexLayoutModule,
        MatIconModule,
        routing,
        AeolixModule,
        AuthModule, SharedModule
    ],
    providers: [
        DAveService,
        TheVoidDataService
    ],
    bootstrap: [AppComponent, AuthComponent],
    schemas: [NO_ERRORS_SCHEMA]
})

export class AppModule {

    /*
    constructor(mdIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
        mdIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('./svg/mdi.svg')); // Or whatever path you placed mdi.svg at
    }
    */
}