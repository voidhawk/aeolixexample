var express = require('express');
var router = express.Router();
var UserMgmt = require('../models/user.js');

router.post('/', function (req, res, next) {

    var email = req.body.email; 
    var user = new UserMgmt({
        email: req.body.email,
        password: req.body.password,
        role: req.body.roles,
        config: req.body.configs
    });

    user.save(function (err, result) {
        if (err) {
            return res.status(500).json({
                title: 'An error occured',
                error: err
            });
        }
        res.status(201).json({
            message: 'Content was saved',
            obj: result
        });
    });
});


module.exports = router;
