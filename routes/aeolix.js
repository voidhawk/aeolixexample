var express = require('express');
var router = express.Router();
var Aeolix = require('../models/aeolix.js');


router.post('/', function (req, res, next) {
    var testCriteria;
    var aeolix = new Aeolix({
        publishResp2Url: req.body.publishResp2Url,
        publish2Url: req.body.publish2Url,
        subType: req.body.subType,
        service: req.body.service,
        creationTime: Date.parse(req.body.creationTime),
        uniqueID: req.body.uniqueID,
        originator: req.body.originator,
        data: req.body.data
    });

    testCriteria = {
        'subType': aeolix.subType,
        'service': aeolix.service,
        'uniqueID': aeolix.uniqueID,
    };

    /*if (req.query.data.responseStatuts) {
    switch (req.body.subType) {
        case 'Tour':
            {
                testCriteria['data.responseStatus.errorCode'] = req.query.data.responseStatuts.errorCode 
            }
        case 'PosEvent':
            {

            }
        case 'Notification':
            {

            }
        case 'Subscription':
            {

            }

    }
}
*/
    Aeolix.find(testCriteria)
        .exec(function (err, result) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occured',
                    error: err
                });
            } else if (result.length == 0) {
                aeolix.save(function (err, result) {
                    if (err) {
                        return res.status(500).json({
                            title: 'An error occured',
                            error: err
                        });
                    }
                    res.status(201).json({
                        message: 'Message was Saved',
                        obj: result
                    });
                });
            } else {                                                                //Checking, if there is already more than one response or request, depending on what you wanna save. We only allow one Response to one reqeust.
                for (var i = 0; i < result.length; i++){
                    if(aeolix.data.source && result[i].data.responseStatus || aeolix.data.responseStatus && result[i].data.source)    {
                        continue;
                    }
                        return res.status(200).json({
                            message: 'Only one pair of cohesive Request/Response'
                        });
                        //break;
                }
                aeolix.save(function (err, result) {
                    if (err) {
                        return res.status(500).json({
                            title: 'An error occured',
                            error: err
                        });
                    }
                    res.status(201).json({
                        message: 'Message was Saved',
                        obj: result
                    });
                });

            }
        });
});




router.get('/selection', function (req, res, next) {

    var findCriteria = {};
    var subfindCriteria = {};
    var selectCriteria = {};
    var subselectCriteria = {};

    var stuff2sendback = [];

    function subfindFunction(req, content, i, j)  {
        switch (req.query.targetSubType) {
            case 'Subscription':
                {

                    subfindCriteria = {
                        'data.subscription.scemid': content[i]._doc.data.tour.stops[j].scemid
                    };
                    break;
                }
            case 'Notification': //ToDo
                {
                    subfindCriteria = {

                    };
                    break;
                }
            case 'PosEvent':
                {
                    subfindCriteria = {
                        'data.posEvent.scemid': content[i]._doc.data.tour.stops[j].scemid
                    }
                    break;
                }
        }
        Aeolix.find(subfindCriteria)
            //.select(subselectCriteria)
            .exec(function (err, result) {
                if (result.length > 0) {
                    stuff2sendback.push(result[0]._doc);
                }
                if (i == content.length - 1 && j == content[i]._doc.data.tour.stops.length - 1) {
                res.status(200).json({
                    message: 'Success',
                    obj: stuff2sendback
                });    
            }
            });
//                for (i = 0; i < content.length; i++) {
//    for (j = 0; j < content[i]._doc.data.tour.stops.length; j++) {


    }

    switch (req.query.targetSubType) {
        case 'Tour':
            {
                findCriteria = {
                    'subType': 'Tour',
                    'data.responseStatus': {
                        $exists: true
                    },
                //    'originator.source': req.query.targetSource
                };

                if (!(req.query.targetUniqueID == "undefined" || req.query.targetUniqueID == "null")) {
                    findCriteria['uniqueID'] = req.query.targetUniqueID;
                }

                selectCriteria = {
                    uniqueID: 1,
                    'data.tour.stops.scemid': 1,
                    creationTime: 1
                };

                break; //suche nach Tour, Antwort mit Request und Response
            }
            
        case 'PosEvent':
            {
                findCriteria = {
                    'uniqueID': req.query.targetUniqueID,
                    'data.responseStatus': {
                        $exists: true
                    }
                };


                selectCriteria = {
                    'data.tour.stops.scemid': 1
                };

                subselectCriteria = {
                    uniqueID: 1,
                    'data.posEvent.scemid': 1
                };

                break; //suche nach PosEvent
            }
        case 'Notification':
            {
                findCriteria = {

                };

                selectCriteria = {

                };
                break; //suche nach Notification        -- ToDo
            }
        case 'Subscription':
            {
                findCriteria = {
                    'uniqueID': req.query.targetUniqueID,

                };

                selectCriteria = {
                    'data.tour.stops.scemid': 1
                };

                subselectCriteria = {
                    uniqueID: 1,
                    'data.subscription.scemid': 1
                };

                break;
            }
    }
    Aeolix.find(findCriteria)
        .select(selectCriteria)
        .exec(function (err, content) {

            if (err) {
                return res.status(500).json({
                    title: 'An error occured',
                    error: err
                });
            }


            if (req.query.targetSubType == 'Tour') {
                res.status(200).json({
                    message: 'Success',
                    obj: content
                });
            } else {
                for (i = 0; i < content.length; i++) {
                    for (j = 0; j < content[i]._doc.data.tour.stops.length; j++) {
                        subfindFunction(req, content, i, j);
                    }
                }

            }

        });
});

router.get('/', function (req, res, next) { //ToDo: einiges

    var findCriteria = {};
    var selectCriteria = {};

    switch (req.query.targetSubType) {
        case 'Tour':
            {
                findCriteria = {
                    'uniqueID': req.query.targetUniqueID
                };
                break;
            }
        case 'Notification':
            {
                findCriteria = {
                    'data.notification.scemid': req.query.targetSCEMID,
                    'data.notification.responseStatus': {
                        $exists: true
                    }
                };
                break;
            }
        case 'PosEvent':
            {
                findCriteria = {
                    'data.posEvent.scemid': req.query.targetSCEMID
                };
                break;
            }
        case 'Subscription':
            {
                findCriteria = {
                    'subType': req.query.targetSubType,
                    'data.scemid': req.query.targetSCEMID
                };
                break;
            }
    }

    Aeolix.find(findCriteria)
        .select({_id : 0})
        .exec(function (err, content) {
            if (err) {
                return res.status(500).json({
                    title: 'An error occured',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Success',
                obj: content
            });
        });

});

module.exports = router;