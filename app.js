var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var appRoutes = require('./routes/app');
var AeolixRoutes = require('./routes/aeolix.js');
var authRoutes =  require('./routes/auth.js');

//var userRoutes = require('./routes/user.js');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});
app.use('/aeolix_server', AeolixRoutes);
app.use('/auth', authRoutes);
app.use('/', appRoutes);

//mongoDB
mongoose.Promise=Promise;

mongoose.connect('mongodb://aeolix:aeolix@ds139352.mlab.com:39352/aeolix',{
  useMongoClient: true,
  promiseLibrary : global.Promise})
        .then(({db: {databaseName}}) => console.log(`Connected to ${databaseName}`))
        .catch(err => console.error(err));
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.render('index');
});

module.exports = app;
